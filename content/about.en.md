---
title: "About"
date: 2020-12-26T17:00:00-05:00
type: "about"
comments: false
---

# Welcome to version 2.0 of my Awesome Blog!

What makes this version 2.0?  Well, I have simply moved from [Hexo](https://hexo.io/) to [Hugo](https://gohugo.io/), and the content has been migrated to a fresh new theme!

This pretty much sums up why I started this blog:

>"It is noble to teach oneself, but still nobler to teach others - and less trouble." -- Mark Twain

I have been here too, so hopefully I'll be able help others to not end up like this guy:
<br />
<br />
![Wisdom of the Ancients](https://imgs.xkcd.com/comics/wisdom_of_the_ancients.png "xkcd: https://imgs.xkcd.com/comics/wisdom_of_the_ancients.png")

Feel free to poke around and check out the [posts](/posts), or try the new search feature above.

Thanks for stopping by!

-- Aaron von Awesome