---
title: "Flush Your DNS Cache (updated 2023)"
date: 2018-10-10T17:50:30-05:00
lastmod: 2023-10-01T11:15:00-05:00
draft: false
comment:
  enable: true
description: "Here are same ways to clear your DNS cache in Linux Mint (updated 2023)"
categories:
  - "Linux Mint"
---

## TL;DR

Here is how you can clear your DNS cache in Linux Mint 21 (updated part):

```bash
sudo resolvectl flush-caches
```

Here are two ways to clear your DNS cache in Linux Mint 19 (for posterity):

**1. Using systemd-resolve**

```bash
sudo systemd-resolve --flush-caches
```

**2. Using dns-clean**

```bash
sudo /etc/init.d/dns-clean start
```

### Source
- https://vitux.com/how-to-flush-the-dns-cache-on-ubuntu/

## The Rest of the Story

So this is more for me to have this documented. Yeah, I could make a bookmark, but you never know ;-)  The original commands for Linux Mint 19 came from [this post](https://vitux.com/how-to-flush-the-dns-cache-on-ubuntu/ "How to Flush the DNS Cache on Ubuntu"), so a big thank you to that author.  I was using Linux Mint 19, and as the original article says, the old commands worked in vanilla Ubuntu 18.04 LTS as well.

The new commands, well, I forget where I originally found them 😇️.

Here are the different ways to clear your DNS cache.

In Linux Mint 21 (updated part):

```bash
sudo resolvectl flush-caches
```

This can be verified by running:

```bash
resolvectl statistics
```

And the results for that should look something like this after clearing the DNS cache:

```bash
DNSSEC supported by current servers: no

Transactions
Current Transactions: 0
  Total Transactions: 19364

Cache
  Current Cache Size: 0
          Cache Hits: 1234
        Cache Misses: 8675

DNSSEC Verdicts
              Secure: 0
            Insecure: 0
               Bogus: 0
       Indeterminate: 0
```

In Linux Mint 19 (for posterity):

**1. Using systemd-resolve**

```bash
sudo systemd-resolve --flush-caches
```

And you can verify that worked by running the command below, and verifying that the "Current Cache Size" is "0" (zero).

```bash
sudo systemd-resolve --statistics
```

**2. Using dns-clean**

```bash
sudo /etc/init.d/dns-clean start
```

## Conclusion

That's it, no need to restart your computer; your DNS cache is clear!

End of Line.
