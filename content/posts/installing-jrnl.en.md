---
title: "Installing JRNL"
date: 2019-09-08T21:50:00-05:00
lastmod: 2021-10-21T12:22:00-05:00
draft: false
comment:
  enable: true
description: "How to install JRNL, a terminal-based journaling application, on Linux Mint."
categories:
  - Journaling
---

***Updated on 2021-10-21 for Linux Mint 20.2***

## TL;DR

To install JRNL on Linux Mint 20.2, open up your Terminal, and type the following commands:

```bash
sudo apt install python3-pycryptodome
sudo apt install python3-setuptools
sudo apt install python3-testresources
sudo apt install python-wheel-common
pip install jrnl[encrypted] --user
```

Add this to the top of your `.bashrc` file: `HISTIGNORE="$HISTIGNORE:jrnl *"`

And to change the tool you use to edit entries, edit the `.config/jrnl/jrnl.yaml` file:

> *Note: If the `jrnl.yaml` file does not exist, just create the file `~/.config/jrnl/jrnl.yaml`, and you can copy the [contents below](#jrnl-yaml) to get started.*

```bash
xed ~/.config/jrnl/jrnl.yaml
```

To edit entries with [Neovim](https://neovim.io/), change the line that starts with "editor" to this: `editor: nvim -c startinsert`

-or-

To edit entries with [Sublime Text](https://www.sublimetext.com/ "Sublime Text Homepage"), change the line that starts with "editor" to this: `editor: subl -w`


## The Rest of the Story

This is just a quick post about [JRNL](https://jrnl.sh/ "JRNL Homepage"). JRNL is an open-source, terminal-based journaling application.  I really like it due to the simplicity, so I\'m giving it a try.  Additionally, I really like the fact that you can encrypt your journal as well :-)

The official installation instructions [can be found here](https://jrnl.sh/en/stable/installation/ "JRNL Installation Instructions").  However, I had to take some additional steps, and wanted to document this in case others had similar issues.

So, to install JRNL on Linux Mint 20.2, open up your Terminal, and type the following commands:

```bash
sudo apt install python3-pycryptodome
sudo apt install python3-setuptools
sudo apt install python3-testresources
sudo apt install python-wheel-common
pip install jrnl[encrypted] --user
```

Then to start your journal, just open up a Terminal, and type `jrnl`.

The very first time, you will asked for the location of your `journal.txt`.  You can name the file whatever you like.  You will also be asked if you want to encrypt your journal as well.  All up to you.

```bash
jrnl
Path to your journal file (leave blank for ~/journal.txt):
~/my-awesome-journal.txt
Enter password for journal (leave blank for no encryption):
Do you want to store the password in your keychain? [Y/n]
Y
Journal will be encrypted.
[Compose Entry; press Ctrl+D to finish writing]
My first entry.
[Entry added to default journal]
```

More about JRNL [can be found here](https://jrnl.sh/en/stable/overview/ "JRNL Overview"), and the GitHub page [can be found here](https://github.com/jrnl-org/jrnl "JRNL GitHub page").

### Bonus

You are also going to want to add this to your `.bashrc` file:

```bash
xed ~/.bashrc
```

Just add this at the top: `HISTIGNORE="$HISTIGNORE:jrnl *"`

This will prevent any JNRL entries for being added to your history file.

### Bonus++

I like to edit entries with [Neovim](https://neovim.io/), and with a simple config file setting, you can do that with JRNL:

> *Note: If the `jrnl.yaml` file does not exist, just create the file `~/.config/jrnl/jrnl.yaml`, and you can copy the [contents below](#jrnl-yaml) to get started.*

```bash
xed ~/.config/jrnl/jrnl.yaml
```

Change the line that starts with "editor" to this: `editor: nvim -c startinsert`

<div id="jrnl-yaml"></div>

```yaml
colors:
  body: none
  date: none
  tags: none
  title: none
default_hour: 9
default_minute: 0
editor: nvim -c startinsert
encrypt: true
highlight: true,
indent_character: '|'
journals:
  default: ~/von-awesome-journal.txt
linewrap: 79
tagsymbols: '@'
template: false
timeformat: '%Y-%m-%d %H:%M'
version: v2.8.3
```

More information on the `jrnl.yaml` config file [can be found here](https://jrnl.sh/en/stable/advanced/#configuration-file).

## Conclusion

So that is how you can get started with JRNL, and to begin journaling your day.

Hope this was helpful :-)  Feel free to leave comments, or questions below.

End of Line.
