---
title: "Troubleshooting in Linux"
date: 2021-12-25T21:17:56-05:00
lastmod: 2021-12-25T21:17:56-05:00
draft: false
comment:
  enable: true
description: "For whatever reason, I keep forgetting the locations to look at, and commands to use, in order to troubleshoot issues on a Linux system.  So this post is for me, so I can reference these all in one place 😇️"
categories:
  - Troubleshooting
---

## TL;DR

Here are some terminal commands, and utilities that can be used for viewing logs in order to troubleshoot your Linux system.

```bash
sudo vi /var/log/syslog
```
```bash
sudo vi /var/log/kern.log
```
```bash
sudo vi /var/log/auth.log
```
```bash
journalctl
```
```bash
sudo journalctl -u ssh
```

In Linux Mint, there is a handy app called `Logs`.  You can find that by clicking on the Linux Mint menu icon, or pressing the Super Key, and then typing "logs".

![Find Logs App](/posts/troubleshooting-in-linux/logs-01.png)

Click on the `Logs` App to open it.

![Logs App](/posts/troubleshooting-in-linux/logs-02.png)

You can then click on the categories of logs on the left to view the given logs (*Important*, *All*, *Applications*, *System*, *Security*, *Hardware*).


## The Rest of the Story

For whatever reason, I keep forgetting the locations to look at, and commands to use, in order to troubleshoot issues on a Linux system.  So this post is for me, so I can reference these all in one place 😇️

And as I learn of more ways to do this, I'll keep adding to the list.

I'll start with the way to do this in the terminal, and then at the end  I'll show the Linux Mint graphical user interface (GUI) tool that can be used.

### Log Files

First off, this seems to be the "go-to" log file to start with, and you can view it with this command:

```bash
sudo vi /var/log/syslog
```
If you don't want to worry about accidentally changing the file (not sure changing a log file will hurt anything) you can run:
```bash
sudo cat /var/log/syslog | less
```
If you suspect some type of kernel panic, you can check this log:
```bash
sudo vi /var/log/kern.log
```
And if you want to check authorizations/logins, you can examine this log:

```bash
sudo vi /var/log/auth.log
```

### Journalctl

If your system (e.g. Linux Mint, Debian, or Ubuntu) is running `systemd`, you can also use this handy tool called `journalctl`.

By running this command, you view an amalgamation of logs, and use the arrow keys to navigate the output:
```bash
journalctl
```
You can also use `journalctl` to examine the logs of a particular `systemd` Unit:
```bash
sudo journalctl -u ssh
```
**Note:** You may not get all the logs without elevated permissions.  That's why you see `sudo` in the command above.

A very helpful video that provides an overview of `journalctl` [can be found here](https://www.youtube.com/watch?v=3kl62YSU9XA&t=420s).

### Log GUI

In Linux Mint, there is a handy app called `Logs`.  You can find that by clicking on the Linux Mint menu icon, or pressing the Super Key, and then typing "logs".

![Find Logs App](/posts/troubleshooting-in-linux/logs-01.png)

Click on the `Logs` App to open it.

![Logs App](/posts/troubleshooting-in-linux/logs-02.png)

You can then click on the categories of logs on the left to view the given logs (*Important*, *All*, *Applications*, *System*, *Security*, *Hardware*).

<br/>

## Conclusion

So there is a short list of starter commands and utilities you can use to troubleshoot your Linux system.

<br/>

End of Line.



