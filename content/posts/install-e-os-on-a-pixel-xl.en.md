---
title: "Install /e/ OS on a Pixel XL"
date: 2019-10-20T12:00:00-05:00
lastmod: 2019-10-20T12:00:00-05:00
draft: false
comment:
  enable: true
description: "A tutorial on how to install /e/ OS and a Pixel XL."
categories:
  - Android
  - eOS
  - Privacy
---

## TL;DR

Basically copying the steps from the [/e/ OS Wiki](https://doc.e.foundation/devices/marlin/install "Install /e/ on a Google Pixel XL"), which copied its steps from the [LineageOS Wiki](https://wiki.lineageos.org/devices/marlin/install "Install LineageOS on marlin").

## The Rest of the Story

### Prerequisites

First, you will need to have `adb` and `fastboot` installed on your system.

If you are using Linux Mint (or another Debian/Ubuntu-based distro), just open a terminal, and enter the following command:

```bash
sudo apt install adb fastboot
```

If you are on Windows, you can follow these instructions:
- https://wiki.lineageos.org/adb_fastboot_guide.html#on-windows

If you are on a Mac, you can following these instructions:
- https://wiki.lineageos.org/adb_fastboot_guide.html#on-macos

<div id="USB-Debugging"></div>

Next, you will need to enable Developer Options, and USB Debugging on your device.  Here are the steps on an **Android 9**, Pixel XL that you will need to take:

1. Open "Settings", and select "About Phone".
![USB Debugging 1](/posts/install-e-os-on-a-pixel-xl/USB-Debugging-01.png " ")
![USB Debugging 2](/posts/install-e-os-on-a-pixel-xl/USB-Debugging-02.png "About Phone")
2. Tap on "Build number" seven times.
![USB Debugging 3](/posts/install-e-os-on-a-pixel-xl/USB-Debugging-03.png "Build Number")
![USB Debugging 4](/posts/install-e-os-on-a-pixel-xl/USB-Debugging-04.png "Developer Mode Enabled")
3. Go back to "Settings", select "System", then "Advanced", and select "Developer options".
![USB Debugging 5](/posts/install-e-os-on-a-pixel-xl/USB-Debugging-05.png "System")
![USB Debugging 6](/posts/install-e-os-on-a-pixel-xl/USB-Debugging-06.png "Advanced")
![USB Debugging 7](/posts/install-e-os-on-a-pixel-xl/USB-Debugging-07.png "Developer Options")
4. Scroll down, and check the "USB debugging" entry under "Debugging".
![USB Debugging 8](/posts/install-e-os-on-a-pixel-xl/USB-Debugging-08.png "USB debugging")
![USB Debugging 9](/posts/install-e-os-on-a-pixel-xl/USB-Debugging-09.png "Allow USB debugging")
![USB Debugging 10](/posts/install-e-os-on-a-pixel-xl/USB-Debugging-10.png "Successfully Enabled USB Debugging")
5. Plug your device into your computer.
6. On the computer, open up a terminal/command prompt, and type adb devices.
```bash
adb devices
List of devices attached
* daemon not running; starting now at tcp:5037
* daemon started successfully
```
7. A dialog should show on your device, asking you to allow usb debugging. Check "Always allow from this computer", and choose "OK".
![USB Debugging 11](/posts/install-e-os-on-a-pixel-xl/USB-Debugging-11.png "Alway allow USB debugging")

### Unlocking the Bootloader

***Warning:*** *After you complete the steps to unlock the bootloader, the phone will perform a Factory Reset!  You will lose* ***ALL*** *the data on you phone, so be prepared!*

#### Step 1 Caveats

There are several things to note about step 1 (one) below.

Firstly, I am preforming these steps on a straight out-of-the-box Pixel XL.  That means the initial settings on the phone are the absolute defaults.  For example, I have **not** updated to Android 10, even though I keep getting the notification.

Secondly, I **had to be connected** to Wi-Fi for the "OEM unlocking" option to be available, otherwise it was grayed out (i.e. disabled).

And thirdly, I need to mention, if you are having trouble with the "OEM unlocking" option not being available, then follow these instructions: [[How-to] Unlock bootloader on Verizon Pixel/XL](https://forum.xda-developers.com/pixel-xl/how-to/how-to-unlock-bootloader-verizon-pixel-t3796030 "[How-to] Unlock bootloader on Verizon Pixel/XL").

#### The Actual Steps

1. Enable "OEM unlocking" by going to "Settings", "System", "Advanced", and then "Developer options".
![USB Debugging 5](/posts/install-e-os-on-a-pixel-xl/USB-Debugging-05.png "System")
![USB Debugging 6](/posts/install-e-os-on-a-pixel-xl/USB-Debugging-06.png "Advanced")
![USB Debugging 7](/posts/install-e-os-on-a-pixel-xl/USB-Debugging-07.png "Developer Options")
![OEM Unlocking 2](/posts/install-e-os-on-a-pixel-xl/OEM-unlocking-02.png "OEM unlocking")
2. Tap "Enable" when presented with this pop-up
![OEM Unlocking 1](/posts/install-e-os-on-a-pixel-xl/OEM-unlocking-01.png "Confirm to Enable OEM unlocking")
3. Connect your Pixel XL to your computer via USB.
4. On the computer, open a Terminal window, and type:
```bash
adb reboot bootloader
```
5. Once the device is in fastboot mode, verify your computer finds it by typing:
```bash
fastboot devices
```
    - If you see `no permissions fastboot`, try running `fastboot` with the `sudo` command.
6. Now type the following command to unlock the bootloader:
```bash
fastboot flashing unlock
```
7. Use the volume up button on the side of the phone to highlight "Yes Unlock bootloader (way void warranty)", then press the power button on the side of the phone to confirm.
![OEM Unlocking 3](/posts/install-e-os-on-a-pixel-xl/OEM-unlocking-03.jpg " ")
8. You will then be brought back to the bootloader screen, and you will see at the bottom: "Device is UNLOCKED".
9. Again, using the volume up/down buttons on the side of the phone, adjust to action selector to read "Power off", and then press the power button on the side of the phone.
![OEM Unlocking 4](/posts/install-e-os-on-a-pixel-xl/OEM-unlocking-04.png "Power off / Reboot device")
10. Wait for the phone to power off completely, and then turn the phone on again.  This boot will take a while as the phone will be performing a factory reset.
11. Since the device resets completely, you will need to [re-enable USB debugging](#usb-debugging) to continue

### Download /e/ OS for the Pixel XL

Download the latest build of /e/ OS for your Pixel by [going here](https://images.ecloud.global/dev/marlin/ "/e/ ROM latest dev build downloads").

To download the latest file, click on the link with the highest date.  The files are formatted this way:
`e-0.7-p-YYYYMMDDXXXXX-dev-marlin.zip`
- YYYY = Year
- MM = Month
- DD = Day
- XXXXX = random build number (I think)

As of this writing, the latest file is: `e-0.7-p-2019110830006-dev-marlin.zip`

<div id="verify-download"></div>

Once you have downloaded the `zip` file, it\'s best to ensure the validity of the file.  To do that, click on the "SHA256" link next to the file you downloaded:
![Download eOS 1](/posts/install-e-os-on-a-pixel-xl/Download-e-os-01.png "Click on SHA256 link")

You will be presented with a screen that looks like this:
![Download eOS 2](/posts/install-e-os-on-a-pixel-xl/Download-e-os-02.png "/e/ OS SHA256 sum")

Now open up a Terminal window, and navigate to the directory where you downloaded the `zip` file, and type the following command:

***Note:*** *Be sure to change the file name to the name of the file you downloaded.  As stated before, the exact file name used in this post is only valid for the time of this post.*

```bash
sha256sum e-0.7-p-2019110830006-dev-marlin.zip
```

Now compare the two outputs to be sure they match:
```bash
3ecd656b032f8ee66eb8a936e0086a817bac6d1793d802956d6d7efdfd540319  e-0.7-p-2019110830006-dev-marlin.zip
```

![Download eOS 2](/posts/install-e-os-on-a-pixel-xl/Download-e-os-02.png "Should Match above Terminal Output")

### Download TWRP Recovery

The version of [LineageOS](https://www.lineageos.org/ "LineageOS Homepage") on which /e/ OS is based, has a built-in recovery.  However, in order to install /e/ OS to begin with you will have to flash a temporary recovery called [TWRP](https://twrp.me/ "TWRP Homepage")

Download the latest version of TWRP for the Pixel XL by [going here](https://dl.twrp.me/marlin/ "TWRP for the Pixel XL").

The version used in this tutorial is: [twrp-3.3.1-3-marlin.img](https://dl.twrp.me/marlin/twrp-3.3.1-3-marlin.img.html "TWRP for marlin").

### Putting It All Together

By now you should have two files:

***Note:*** *Exact file versions may be different.  Please remember to download the latest version of each file.*

1. twrp-3.3.1-3-marlin.img
1. e-0.7-p-2019110429547-dev-marlin.zip

Here are the steps you will need to take in order install everything you downloaded onto your Pixel XL:

1.  Power on your Pixel XL.
2.  Once the phone has booted into the homescreen, connect your Pixel XL to your computer via USB.
3.  On the computer, open up a Terminal window, and type the following:
```bash
adb reboot bootloader
```
4. Your Pixel XL should automatically reboot into the bootloader menu.
![Install e 1](/posts/install-e-os-on-a-pixel-xl/install-e-01.png "Bootloader Menu")
5. Verify that your computer can still detect the Pixel XL whilst at the bootloader menu.
    - In the Terminal window, type the following:
      ```bash
      fastboot devices
      ```
    - Your output should look something like this:
      ```bash
      HTXXXXXXXX60    fastboot
      ```
      ***Note:*** *If you see "no permissions fastboot", try running fastboot as root using the `sudo` command.*
6. Now you can temporarily flash the [TWRP](https://twrp.me/ "TWRP Homepage") recovery onto your Pixel XL by doing the following:
    - In the Terminal, navigate to the directory where you downloaded the `twrp-3.3.1-3-marlin.img` file.
    - Type the following to push the file to your Pixel XL:
      ```bash
      fastboot flash boot twrp-3.3.1-3-marlin.img
      ```
      ***Note:*** *The file may not be named identically to what is shown in the command above, so adjust accordingly.*
    - Results from executing the fastboot command should look similar to this:
      ```bash
      downloading 'boot.img'...
      OKAY [  0.872s]
      booting...
      OKAY [  1.051s]
      finished. total time: 1.923s
      ```
7. Your Pixel XL will return to the bootloader menu:
![Install e 1](/posts/install-e-os-on-a-pixel-xl/install-e-01.png "Bootloader Menu")
8. Use the volume down button on the side of the phone to change the selection to "Recovery mode", then push the power button on the side of the phone to select that option
9. Your Pixel XL will reboot, and then enter the TWRP recovery:
![Install e 3](/posts/install-e-os-on-a-pixel-xl/install-e-03.png " ")
![Install e 4](/posts/install-e-os-on-a-pixel-xl/install-e-04.png " ")
10. At the TWRP main menu, tap "Wipe"
![Install e 5](/posts/install-e-os-on-a-pixel-xl/install-e-05.png " ")
11. Now tap "Format Data", and continue with the formatting process by typing "yes" then tapping the check mark on the keyboard. This will remove encryption as well as delete all files stored on the internal storage.
![Install e 6](/posts/install-e-os-on-a-pixel-xl/install-e-06.png " ")
![Install e 7](/posts/install-e-os-on-a-pixel-xl/install-e-07.png " ")
12. Return to the previous menu by tapping the "Back" button, and then the back triangle
![Install e 8](/posts/install-e-os-on-a-pixel-xl/install-e-08.png " ")
![Install e 9](/posts/install-e-os-on-a-pixel-xl/install-e-09.png " ")
13. Now tap "Advanced Wipe"
![Install e 10](/posts/install-e-os-on-a-pixel-xl/install-e-10.png " ")
14. Select all of the options, and then "Swipe to Wipe"
![Install e 11](/posts/install-e-os-on-a-pixel-xl/install-e-11.png " ")
    ***Note:*** *If you see `Error opening: '/data/media' (No such file or directory)`, you can continue, don't worry.*
15. Now tap "Back", then tap the back triangle twice to get back to the main menu.
16. Tap "Reboot", then tap "Recovery", and "Swipe to Reboot".
![Install e 15](/posts/install-e-os-on-a-pixel-xl/install-e-15.png " ")
![Install e 16](/posts/install-e-os-on-a-pixel-xl/install-e-16.png " ")
![Install e 17](/posts/install-e-os-on-a-pixel-xl/install-e-17.png " ")
17. The Pixel XL will reboot, and return to the TWRP main menu
18. Tap "Advanced" on the TWRP main menu, then "ADB Sideload"
![Install e 18](/posts/install-e-os-on-a-pixel-xl/install-e-18.png " ")
![Install e 19](/posts/install-e-os-on-a-pixel-xl/install-e-19.png " ")
19. Select "Wipe Dalvik Cache", "Wipe Cache", and then "Swipe to Start Sideload".
![Install e 20](/posts/install-e-os-on-a-pixel-xl/install-e-20.png " ")
20. On your computer you now start the sideload of /e/ OS:
    - In the Terminal, navigate to the directory where you downloaded the `e-0.7-p-2019110830006-dev-marlin.zip` file.
    - Type the following to push the file to your Pixel XL:
      ```bash
      adb sideload e-0.7-p-2019110830006-dev-marlin.zip
      ```
      ***Note:*** *The file may not be named identically to what stands in this command, so adjust accordingly.*
    - You will see progress in the Terminal, and on your Pixel XL:
      ```bash
      serving: 'e-0.7-p-2019110830006-dev-marlin.zip'  (`~`XX%)
      ```
21. Now tap "Reboot System", then "Swipe to Reboot", and your Pixel XL will boot into your newly installed /e/ OS. Follow the on-screen instructions to set up you Pixel XL, and you\'re ready to start experimenting.
![Install e 21](/posts/install-e-os-on-a-pixel-xl/install-e-21.png " ")

## Conclusion

There you have it, you now have the privacy-respecting /e/ OS on your Pixel XL.  Proceed to the bonus content if you want to root your Pixel XL.  If you do not want to root your device, then you are done!

Help you found this helpful!

End of Line.
