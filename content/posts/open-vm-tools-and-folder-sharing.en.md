---
title: "Open VM Tools and Folder Sharing"
date: 2018-09-04T17:52:28-05:00
lastmod: 2018-09-04T17:52:28-05:00
draft: false
comment:
  enable: true
description: "If you are using Open VMWare Tools on a Linux Mint (or Ubuntu) virtual machine, here is how you get the 'Shared Folders' to work between the host operating system (Windows 10 in my case), and the host operating system (Linux Mint 19)."
categories:
  - VMWare
  - Virtualization
---

## TL;DR

If you are using Open VMWare Tools on a Linux Mint (or Ubuntu) virtual machine, here is how you get the "Shared Folders" to work between the host operating system (Windows 10 in my case), and the host operating system (Linux Mint 19):

**Note:** Run these commands from the guest operating system.

1. Make sure you have Open VMWare Tools installed on your guest operating system.  If not, install it, and then reboot your guest virtual machine.
```bash
sudo apt update
sudo apt install open-vm-tools open-vm-tools-desktop
```

2. Create the directory in which the shared folders will be accessed on the guest operating system:
```bash
sudo mkdir -p /mnt/hgfs
```

3. Mount the file system, and you should then be able to see the folders you have shared in the guest operating system in the directory `/mnt/hgfs`:
```bash
sudo mount -t fuse.vmhgfs-fuse .host:/ /mnt/hgfs -o allow_other
```

4. Finally, in order to get the shared folders to automatically mount every time you boot up your virtual operating system, add the following line to `/etc/fstab`:
```bash
.host:/ /mnt/hgfs   fuse.vmhgfs-fuse    auto,allow_other  0   0
```

### Source
Steps taken from [this post](https://gist.github.com/darrenpmeyer/b69242a45197901f17bfe06e78f4dee3 "open-vm-tools and VMWare Shared Folders for Ubuntu guests").

## The Rest of the Story

I recently decided to upgrade, and virtualize my Desktop environment. The outcome vision is basically all my day-to-day activities will run on my Linux Mint 19 virtual machine, and leave my Windows 10 host machine for gaming only.

Ironic that I went to this extent to get back into PC gaming, and then I heard about [Steam Play Beta](https://itsfoss.com/steam-play/ "How to install Steam Play beta"), which allows crap-ton of Windows-only games to now be played on Linux via Steam. Oh well...maybe when Linux Mint 20 gets released, I'll redo my setup; I'm too far in now ;-)

Ok, jumping out of that rabbit hole, here is what I wanted to share. Usually when I install a guest operating system using VMWare, I immediately install the VMWare Tools that are included with, in my case, VMWare Player for integration between the guest and host operating system. However, this time, after installing Linux Mint 19 as the guest operating system, I was prompted to install the [Open VMWare Tools](https://github.com/vmware/open-vm-tools "Open VMWare Tools"). Now these have [been around for a while](https://blogs.vmware.com/vsphere/2015/09/open-vm-tools-ovt-the-future-of-vmware-tools-for-linux.html "Open VMWare Tools blog post"), but I never bothered to use them until now.

I have had issues updating the standard VMWare Tools on my guest operating systems in the past after kernel updates, plus it's a manual process, but with Open VMWare Tools it's all taking care of by the Linux Mint (guest operating system) package manager.

However, upon trying to use "Shared Folders" (see screenshot), I was unable to find the folders on the guest operating system (`/mnt/hgfs`).
<br />
<br />
![VMWare Shared Folders 1](/posts/open-vm-tools-and-folder-sharing/VMWare-Shared-Folders-01.png "VWMare Shared Folders")

After some searching, I ran across [this post](https://gist.github.com/darrenpmeyer/b69242a45197901f17bfe06e78f4dee3 "open-vm-tools and VMWare Shared Folders for Ubuntu guests"), and the following steps to get VMWare Shared Folders to work for Linux Mint 19 (or Ubuntu) guest operating systems with Open VMWare Tools are taken from that post.  Many thanks to the [author](https://gist.github.com/darrenpmeyer "darrenpmeyer") for the [original post](https://gist.github.com/darrenpmeyer/b69242a45197901f17bfe06e78f4dee3 "open-vm-tools and VMWare Shared Folders for Ubuntu guests").

Just to make sure I'm clear my host operating system (physical PC) is Windows 10, and my guest operating system (VMWare virtual machine) is Linux Mint 19.

### Steps to Access Shared Folders

**Note:** Run these commands from the guest operating system, in my case, Linux Mint 19

1. Make sure you have Open VMWare Tools installed on your guest operating system.  If not, install it, and then reboot your guest virtual machine.
```bash
sudo apt update
sudo apt install open-vm-tools open-vm-tools-desktop
```

2. Create the directory in which the shared folders will be accessed on the guest operating system:
```bash
sudo mkdir -p /mnt/hgfs
```

3. Mount the file system, and you should then be able to see the folders you have shared in the guest operating system in the directory `/mnt/hgfs`:
```bash
sudo mount -t fuse.vmhgfs-fuse .host:/ /mnt/hgfs -o allow_other
```
<br />
![VMWare Shared Folders 2](/posts/open-vm-tools-and-folder-sharing/VMWare-Shared-Folders-02.png "VWMare Shared Folders in File Manager")

4. Finally, in order to get the shared folders to automatically mount every time you boot up your virtual operating system, add the following line to `/etc/fstab`:
```bash
.host:/ /mnt/hgfs   fuse.vmhgfs-fuse    auto,allow_other  0   0
```
<br />
![VMWare Shared Folders 3](/posts/open-vm-tools-and-folder-sharing/VMWare-Shared-Folders-03.png "Auto-mount VMWare Shared Folders Directory")

That's all there is to it. Feel free to leave comments, or questions below.

End of Line.
