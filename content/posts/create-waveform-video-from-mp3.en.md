---
title: "Create Waveform Video from MP3"
date: 2021-05-30T00:21:00-04:00
lastmod: 2021-05-30T00:21:00-04:00
draft: false
comment:
  enable: true
description: "Create a waveform video from an MP3 file using ffmpeg."
categories:
  - "Video Editing"
  - "Audio Editing"
---

## TL;DR

If your MP3 file is called `my-sound-file.mp3`, and your background image is called `my-backgroud-picture.png`, just open up a terminal and run the follow command below to merge them together, and create a fancy waveform video.

```bash
ffmpeg -i my-sound-file.mp3 -i my-background-picture.png -filter_complex \
    "[1:v]scale=1920x1080[image]; \
    [0:a]showwaves=s=1920x1080:colors=blue:mode=line:rate=25[waves]; \
    [image][waves]overlay[combine]" \
    -map "[combine]" -map 0:a mp3-plus-waves-output.mp4
```

### Sources
- https://ffmpeg.org/ffmpeg-filters.html#showwaves
- https://trac.ffmpeg.org/wiki/Waveform#Waveformvideo
- https://www.extua.pw/blog/2018/11/25/ffmpeg_audio_visualisation/

<br/>

## The Rest of the Story

So picture this. You do a live stream, and the video streams horribly, but there is a backup audio recording, and that turns out perfect. So what can you upload to YouTube?  How about a custom background image and the audio overlayed with the waveforms of the speaker's voice?

Now, I am sure this can be done with something like [OpenShot](https://www.openshot.org/) or [KDEN Live](https://kdenlive.org/en/), but I'm just looking for something quick and simple.

I'll admit however, it probably took me longer, because I'm not very familiar with `ffmpeg`, but now that I have this script, it will be extremely simple to do this again the future. Or [will it](https://xkcd.com/1319/)? 😉️

### The Script

So open up your terminal, and paste this in (changing the file names to match your files of course):

```bash
ffmpeg -i my-sound-file.mp3 -i my-background-picture.png -filter_complex \
    "[1:v]scale=1920x1080[image]; \
    [0:a]showwaves=s=1920x1080:colors=blue:mode=line:rate=25[waves]; \
    [image][waves]overlay[combine]" \
    -map "[combine]" -map 0:a mp3-plus-waves-output.mp4
```

#### The Breakdown

Here is what each line does.

1. `ffmpeg -i my-sound-file.mp3 -i my-background-picture.png -filter_complex \`
    - This sets the input sound and image file for the script.  Be sure to change `my-sound-file.mp3` to the name of your sound file, and the same goes for the background image file (`my-background-picture.png`).
2. `"[1:v]scale=1920x1080[image]; \`
    - Scale the background image `[1:v]`(`my-background-picture.png`) to 1920 by 1080, and call it `image`.
3. `[0:a]showwaves=s=1920x1080:colors=blue:mode=line:rate=25[waves]; \`
    - Take the audio file `[0:a]`(`my-sound-file.mp3`), and run it through [showwaves](https://ffmpeg.org/ffmpeg-filters.html#showwaves), set the size to 1920 by 1080, set the color of the waveforms to blue, generate the waveform from a line that run across the screen, set the output frame rate, and call that `waves`.
4. `[image][waves]overlay[combine]" \`
    - Overlay the waveforms over the image, and call it `combine`.
5. `-map "[combine]" -map 0:a mp3-plus-waves-output.mp4`
    - Merge it all together, and churn out the final product, `mp3-plus-waves-output.mp4` (you can call it whatever you like, just keep the `.mp4` extension).

## Conclusion

There you have it; you should have your very own waveform video with a custom background like this one:
<br/>
{{< video "tiger-uppercut.mp4" false >}}

<br/>
<br/>
End of Line.
