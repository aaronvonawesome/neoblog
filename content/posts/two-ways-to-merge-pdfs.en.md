---
title: "Two Ways to Merge PDFs"
date: 2018-12-27T20:14:08-05:00
lastmod: 2018-12-27T20:14:08-05:00
draft: false
comment:
  enable: true
description: "Here are two (of many I'm sure) ways to merge PDFs together."
categories:
  - "PDF Tricks"
---

## TL;DR

Here's how you merge two PDF documents together.

### pdfunite

Merge two documents:

```bash
pdfunite file1.pdf file2.pdf mergedfile.pdf
```

### pdftk

Merge one document with odd pages, and one document with even pages into one sequential file:

```bash
pdftk A=odd.pdf B=even.pdf shuffle A B output merged.pdf
```

## The Rest of the Story

Do you only have a one-sided scanner? Have you ever needed merge two PDF scans together? Here are a couple options for you if you are using Linux.

In my scenario, these commands work on Linux Mint 19.1, but I'll wager that they will work on most if not all Linux distributions (especially the snap package).

### Merge
First I'll handle the simple scenario of merging *file1.pdf* with *file2.pdf*.  For that we'll use `pdfunite`.  Pdfunite came installed with Linux Mint. To use it simply open up the terminal, and follow this pattern:
```bash
pdfunite file1.pdf file2.pdf mergedfile.pdf
```
You can merge as many documents as you want.  You could do:
```bash
pdfunite file1.pdf file2.pdf file3.pdf mergedfile.pdf
```
Or:
```bash
pdfunite file1.pdf file2.pdf file3.pdf file4.pdf mergedfile.pdf
```

Hopefully, you get the idea ;-)  The very last file name in the series will be the merged file, or the output file.  So simply enter the command, then a file name, a space, another file name, a space, etc., and finally the name of the file you want to create.

Here are some more details for the `pdfunite` command: http://manpages.ubuntu.com/manpages/bionic/man1/pdfunite.1.html

### Mixed
Ok, now what if, for example, you only have a one-sided scanner? You now have to scan two-sided documents twice, and that gives you a scan set of odd pages, and a scan set of even pages. Ultimately, you want those to be one sequential document just like the original documents, well there's an app for that too.

The app is called `pdftk`.  This did not come pre-installed with Linux Mint.  So, if you try to run `pdftk` from the terminal, you get this error:

![Install pdftk](/posts/two-ways-to-merge-pdfs/Install-pdftk-01.png "Command 'pdftk' not found")
If you do **not** have Snapd installed, here are the commands to get `pdftk` installed:
```bash
sudo apt install snapd
sudo snap install pdf
```

If you **do** have Snappy installed, here are the commands to get `pdftk` installed:
```bash
sudo snap install pdf
```

Once installed, here is the command to take a scan of odd pages, a scan of even pages, and merge them into one sequential PDF:
```bash
pdftk A=odd.pdf B=even.pdf shuffle A B output merged.pdf
```

In the command above, I actually renamed my file with the odd pages "odd.pdf", and likewise with the even-paged document.  The "merged.pdf" is the file you want to create.  Originally, I found the info for `pdftk` on [StackExchange](https://superuser.com/a/728438 "pdftk post").

And that's it, that's all you need to do.  Hope you find this helpful!

End of Line.
