---
title: "Nifty Virtualbox Commands"
date: 2021-05-07T17:45:00-04:00
lastmod: 2021-05-07T17:45:00-04:00
draft: false
comment:
  enable: true
description: "Start up and connect to VirutalBoxes that do not reside on your machine."
categories:
  - VirtualBox
  - Virtualization
---

## TL;DR

Here are some nifty VirtualBox commands I just want to have for reference 😇

---

List the VirtualBoxes on your machine:
```bash
vboxmanage list vms
```

<br/>

Start your VirtualBox in headless mode (i.e. no UI):
```bash
vboxmanage startvm <vmname> --type headless
```

<br/>

List all of your running VirtualBoxes:
```bash
vboxmanage list runningvms
```

<br/>

List the IP address of the VirtualBox:
```bash
vboxmanage guestproperty enumerate <vmname> --patterns *V4/IP*
```

<br/>

## The Rest of the Story

Do you have a desktop or laptop that you use for running VirtualBoxes?  I'm not talking about a virtual machine server, just a beefed-up PC that runs some of your VirtualBoxes.  And what happens if you are remote, and all you can do is SSH into that PC, how do you start your VirtualBoxes?

Well, if this sounds familiar, here are some nifty commands to start up your VirtualBoxes remotely.

I am assuming that you have connected to a remote PC using something like Secure Shell Protocol (SSH).  You can run these commands locally as well, but my use case is with a remote PC.

I am also assuming that your VirtualBox, the one you want to start up remotely, has remote-connection software set up on it as well.  Maybe SSH, or if it is a Windows VirtualBox, then Remote Desktop (RDP).

<br/>

For starters, I can never seem to remember all the names of my VirtualBoxes, so let's begin by listing the VirtualBoxes on our machine:

```bash
vboxmanage list vms
```
You should then see the VirtualBox names listed:
```bash
"Linux-Mint-20-VirtualBox" {u1ea45f9-u1c5-abce-a55c-u82u46a570ae}
"Von-Awesome-VirtualBox" {5ceuc4b0c-1p58-d76b-98t3-i5uh713q4742}
```

<br/>

Next you'll want to start one of your VirtualBoxes.  To do that, run this command:
```bash
vboxmanage startvm <vmname> --type headless
```

It is important to use `--type headless` here, otherwise the VirtualBox will not start unless you have the VirtualBox GUI running.

Using my list of VirtualBoxes, my command would look like this:
```bash
vboxmanage startvm Linux-Mint-20-VirtualBox --type headless
```

<br/>

Thirdly, you can use this command to see if the VirtualBox is running:
```bash
vboxmanage list runningvms
```

<br/>

If you have a static IP address assigned to your VirtualBox, you can skip this.  But if you do not, this command will list the IP address of the VirtualBox:
```bash
vboxmanage guestproperty enumerate <vmname> --patterns *V4/IP*
```

Again, using my VirtualBox's name, the command would be:
```bash
vboxmanage guestproperty enumerate Linux-Mint-20-VirtualBox --patterns *V4/IP*
```

And the results, will look something like this:
```bash
Name: /VirtualBox/GuestInfo/Net/0/V4/IP, value: 10.0.0.125, timestamp: 1420424562652432112, flags:
```

<br/>

## Conclusion

You should now be able to connect to your remotely-started VirtualBox.

<br/>

End of Line.
