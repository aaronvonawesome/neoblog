---
title: "X11vnc and Linux Mint 19"
date: 2019-09-13T18:00:00-05:00
lastmod: 2019-09-13T18:00:00-05:00
draft: false
comment:
  enable: true
description: "How remote control your Linux Mint PC using VNC."
categories:
  - VNC
  - "Remote Administration"
---

## TL;DR

Here are the steps to install X11vnc, and tunnel connections to remote machines through SSH.

### Install and Configure SSH

See this post: [Setup SSH for Local Access]({{< relref "setup-ssh-for-local-access.en.md" >}}).

### Install and Configure X11vnc

Firstly, I'd like the thank the [author of this article](https://unlockforus.com/remotely-control-linux-mint-18-x-vnc-server-x11vnc-setup/ "Remotely control Linux Mint 18.x – VNC Server (x11vnc) Setup"), as the majority of the steps below come from his (or her) original post.

Install `x11vnc`:

```bash
sudo apt install x11vnc
```

Create the directory that will hold the password and configuration files:

```bash
sudo mkdir /etc/x11vnc
```

Create a password for the X11vnc server:

```bash
sudo x11vnc --storepasswd /etc/x11vnc/vncpwd
Enter VNC password:
Verify password:
Write password to /etc/x11vnc/vncpwd?  [y]/n y
Password written to: /etc/x11vnc/vncpwd
```

Create a `systemd` service file for the X11vnc service:

```bash
sudo xed /lib/systemd/system/x11vnc.service
```

Copy the text below into the `x11vnc.service` file:

```ini
[Unit]
Description=VNC Server for X11
Requires=display-manager.service
After=display-manager.service

[Service]
Type=forking
ExecStart=/usr/bin/x11vnc -dontdisconnect -localhost -auth guess -forever -shared -noxdamage -repeat -rfbauth /etc/x11vnc/vncpwd -rfbport 5900 -bg -o /var/log/x11vnc.log
ExecStop=/usr/bin/killall x11vnc
Restart=on-failure
Restart-sec=5

[Install]
WantedBy=multi-user.target
```

Save and close the service file created, and copy it to `/etc/systemd/system/`:

```bash
sudo cp /lib/systemd/system/x11vnc.service /etc/systemd/system/
```

Now edit the `graphical.target` file:

```bash
sudo xed /lib/systemd/system/graphical.target
```

The final version will look like this:

```ini
# SPDX-License-Identifier: LGPL-2.1+
#
# This file is part of systemd.
#
# systemd is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation; either version 2.1 of the License, or
# (at your option) any later version.

[Unit]
Description=Graphical Interface
Documentation=man:systemd.special(7)
Requires=multi-user.target
Wants=display-manager.service x11vnc.service
Conflicts=rescue.service rescue.target
After=multi-user.target rescue.service rescue.target display-manager.service
AllowIsolate=yes
```

Save and close the `graphical.target` file, and copy it to `/etc/systemd/system/`:

```bash
sudo cp /lib/systemd/system/graphical.target /etc/systemd/system/
```

Enable the X11vnc service to start up on boot, and to kick off the service now:

```bash
sudo systemctl daemon-reload
sudo systemctl enable graphical.target
sudo systemctl start x11vnc.service
```

### Connecting to the X11vnc Host

***Heed and Read:*** *The Following steps should be done on the PC that will **connect to** the X11vnc host.*
<br />
<br />
Open up your Terminal, and type the following:

```bash
sudo apt-add-repository ppa:remmina-ppa-team/remmina-next
sudo apt update
sudo apt install remmina remmina-plugin-vnc remmina-plugin-rdp remmina-plugin-secret remmina-plugin-spice
```

Using the Linux Mint menu find "remmina" to open the main connection window:

![Remmina 1](/posts/x11vnc-and-linux-mint-19/remmina-01.png "Open Remmina")

![Remmina 2](/posts/x11vnc-and-linux-mint-19/remmina-02.png "Remmina Main Window")

Now add a new VNC Connection Profile:

![Remmina 3](/posts/x11vnc-and-linux-mint-19/remmina-03.png "Create a Remmina Connection Profile")

![Remmina 4](/posts/x11vnc-and-linux-mint-19/remmina-04.png "Remmina Connection Profile Settings")

Open a Terminal window, and enter the follow command to establish an SSH connection:

***Note:*** *If you have not followed [the steps in my other post]({{< relref "setup-ssh-for-local-access.en.md" >}}), you'll need to do that first, and the come back to this.*

```bash
ssh -L 5901:127.0.0.1:5900 chucknorris@192.168.77.4 -p 2019
```

Double-click the Connection Profile you created earlier, and remotely connect to your X11vnc host:

![Remmina 6](/posts/x11vnc-and-linux-mint-19/remmina-06.png "Double-click Connection Profile")

![Remmina 7](/posts/x11vnc-and-linux-mint-19/remmina-07.png "Connection to Remote X11vnc Host")

## The Rest of the Story

I've tried several different ways to remote control my Linux Mint PCs at home, and have not found a way I *really* like.  Being honest, I really like Windows Remote Desktop (RDP), because it creates a "private" session; meaning if I am remotely connecting to my host PC, no one in the vicinity of my host PC can see what I am doing if the monitor happens to be on.  Windows RDP will "lock" the host PC, and when I am back in front of the physical PC I can pick right back up where I have left off.

I'm still playing around with [X2Go](https://wiki.x2go.org/doku.php "X2Go Homepage"), but unfortunately X2Go [does not play nice with Cinnamon](https://wiki.x2go.org/doku.php/doc:de-compat#cinnamon "X2Go Desktop Compatibility") (the primary Linux Mint desktop environment).

I have also been unsuccessful with xRDP, but I'll keep experimenting with xRDP on Linux Mint.

If you have been able to get X2Go or xRDP to work on Linux Mint (with Cinnamon), please leave a comment on how you got that to work :-)

### X11vnc + SSH

What I have been able to get working is Virtual Network Computing (VNC), specifically [X11vnc](https://en.wikipedia.org/wiki/X11vnc "Wikipedia: X11VNC").

I use this in conjunction with SSH (TCP forwarding & connection forwarding enabled).  This will enable a secure connection with the VNC host over a secure SSH tunnel.

In this post, I will be taking you through the following:
1. Install and Configure SSH
1. Install and Configure X11vnc
1. Connecting to the X11vnc Host

### Install and Configure SSH

I have a post I wrote on this with all the details, and you can [find that post here]({{< relref "setup-ssh-for-local-access.en.md" >}}).

### Install and Configure X11vnc

Firstly, I'd like the thank the [author of this article](https://unlockforus.com/remotely-control-linux-mint-18-x-vnc-server-x11vnc-setup/ "Remotely control Linux Mint 18.x – VNC Server (x11vnc) Setup"), as the majority of the steps below come from his (or her) original post.

Ok, so now let's install the `x11vnc` server.

Start by opening a Terminal window, and entering the following command:

```bash
sudo apt install x11vnc
```

Now that X11vnc server is installed, create the directory that will hold the password and configuration files.  This will be a system-wide accessible folder.  In the Terminal window, type the following command:

```bash
sudo mkdir /etc/x11vnc
```

<div id="set-vnc-password"></div>

Next, create a password for the X11vnc server.  VNC has a password limit of eight (8) characters *(lame)*, so keep that in mind.  Additionally, you will be prompted to enter the password twice.

```bash
sudo x11vnc --storepasswd /etc/x11vnc/vncpwd
Enter VNC password:
Verify password:
Write password to /etc/x11vnc/vncpwd?  [y]/n y
Password written to: /etc/x11vnc/vncpwd
```

You are going to want the X11vnc server to start on boot, even if there is not a logged-in User.  To accomplish that, create a `systemd` service file for the X11vnc service.

```bash
sudo xed /lib/systemd/system/x11vnc.service
```

And copy the text below into the `x11vnc.service` file:

```ini
[Unit]
Description=VNC Server for X11
Requires=display-manager.service
After=display-manager.service

[Service]
Type=forking
ExecStart=/usr/bin/x11vnc -dontdisconnect -localhost -auth guess -forever -shared -noxdamage -repeat -rfbauth /etc/x11vnc/vncpwd -rfbport 5900 -bg -o /var/log/x11vnc.log
ExecStop=/usr/bin/killall x11vnc
Restart=on-failure
Restart-sec=5

[Install]
WantedBy=multi-user.target
```

You'll notice in the text above, the command parameter `-localhost`.  This will force you to use an SSH tunnel in order to connect to your X11vnc host machine. Ultimately, only use your VNC connection over a "trusted" network, but if you were to enable this on a laptop, you want to be confident that you are **not** exposing your listening VNC connection on an "untrusted" network, for example if you were to connect to the Internet at a coffee shop.

If you followed the instructions in the "[Install and Configure SSH](#install-and-configure-ssh)" section above, you'll have your firewall set up to only allow Local Area Network (LAN) connections to your host machine, and adding the `-localhost` parameter will afford an extra layer of security specific to your VNC connection.

For more information on the `-localhost` parameter, you can [check out this post](https://wiki.archlinux.org/index.php/X11vnc#SSH_Tunnel "Arch Linux Wiki").

Save and close the service file created, and copy it to `/etc/systemd/system/`.

```bash
sudo cp /lib/systemd/system/x11vnc.service /etc/systemd/system/
```

Now edit the `graphical.target` file.

```bash
sudo xed /lib/systemd/system/graphical.target
```

You'll change the line:

```ini
Wants=display-manager.service
```
to:
```ini
Wants=display-manager.service x11vnc.service
```

The full, final version will look like this:

```ini
# SPDX-License-Identifier: LGPL-2.1+
#
# This file is part of systemd.
#
# systemd is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation; either version 2.1 of the License, or
# (at your option) any later version.

[Unit]
Description=Graphical Interface
Documentation=man:systemd.special(7)
Requires=multi-user.target
Wants=display-manager.service x11vnc.service
Conflicts=rescue.service rescue.target
After=multi-user.target rescue.service rescue.target display-manager.service
AllowIsolate=yes
```

Save and close the `graphical.target` file, and copy it to `/etc/systemd/system/`:

```bash
sudo cp /lib/systemd/system/graphical.target /etc/systemd/system/
```

Finally, run the following commands in order to enable the X11vnc service to start up on boot, and to kick off the service now so you can move on to the next section.

```bash
sudo systemctl daemon-reload
sudo systemctl enable graphical.target
sudo systemctl start x11vnc.service
```

### Connecting to the X11vnc Host

There are multiple tools that you can use to connect to your X11vnc host.  My favorite is [Remmina](https://remmina.org/ "Remmina Homepage").  So here are the steps to get Remmina installed, and how to use it to remotely administer you X11vnc host.

The Remmina website has an excellent [How to install Remmina](https://remmina.org/how-to-install-remmina/ "How to install Remmina") guide, however it will be sightly different to make sure the VNC plugin gets installed as well.

**_Heed and Read: The Following steps should be done on the PC that will \*connect to\* the X11vnc host._**

<div id="install-remmina"></div>

Open up your Terminal, and type the following:

```bash
sudo apt-add-repository ppa:remmina-ppa-team/remmina-next
sudo apt update
sudo apt install remmina remmina-plugin-vnc remmina-plugin-rdp remmina-plugin-secret remmina-plugin-spice
```

***Note:*** *There is also a [Remmina snap package](https://snapcraft.io/remmina "Remmina snap package") if you prefer to go that route.  Obviously, the instructions above do not apply to the snap package install.*

With Remmina installed, create a Connection Profile in Remmina.

Using the Linux Mint menu find "remmina" to open the main connection window.

![Remmina 1](/posts/x11vnc-and-linux-mint-19/remmina-01.png "Open Remmina")

![Remmina 2](/posts/x11vnc-and-linux-mint-19/remmina-02.png "Remmina Main Window")

Now add a new VNC Connection Profile.  For now just copy the settings (except the name, name it whatever you wish) in the screenshots below.

![Remmina 3](/posts/x11vnc-and-linux-mint-19/remmina-03.png "Create a Remmina Connection Profile")

![Remmina 4](/posts/x11vnc-and-linux-mint-19/remmina-04.png "Remmina Connection Profile Settings")

Here is brief explanation of the settings in the above screenshot:

- **Protocol**: Is a drop-down menu, and you will want to select "VNC - VNC viewer".  This is why `remmina-plugin-vnc` was used in [the install command above](#install-remmina).
- **Server**: Set this to "localhost:5901".  This works in conjunction with the SSH tunnel; forcing VNC to connect through the SSH tunnel.
- **User password**: You [set the VNC password on this step](#set-vnc-password), that is the same password you would use here.
- **Color depth** and **Quality**: Those settings are totally up to you.

After entering the settings, don't forget to click "Save".  You should see your Connection Profile in the Main Connection Window.

![Remmina 5](/posts/x11vnc-and-linux-mint-19/remmina-05.png "Available Connection Profiles")

Now open a Terminal window, and enter the follow command to establish an SSH connection.  You will be prompted for a password, and this is the password of the User account on the *remote* PC.

***Note:*** *If you have not followed [the steps in my other post]({{< relref "setup-ssh-for-local-access.en.md" >}}), you'll need to do that first, and the come back to this.*

```bash
ssh -L 5901:127.0.0.1:5900 chucknorris@192.168.77.4 -p 2019
```

The first time connecting you will receive this message:

```bash
The authenticity of host '[192.168.77.4]:2019 ([192.168.77.4]:2019)' can't be established.
ECDSA key fingerprint is SHA256:xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.
Are you sure you want to continue connecting (yes/no)? yes
Warning: Permanently added '[192.168.77.4]:2019' (ECDSA) to the list of known hosts.
chucknorris@192.168.77.4's password:
Last login: Thu Sep 12 22:59:54 2019 from 192.168.77.5
```

Now that the SSH connection is established, you can double-click the Connection Profile you created earlier, and remotely connect to your X11vnc host.

![Remmina 6](/posts/x11vnc-and-linux-mint-19/remmina-06.png "Double-click Connection Profile")

![Remmina 7](/posts/x11vnc-and-linux-mint-19/remmina-07.png "Connection to Remote X11vnc Host")

## Conclusion

With the connection established, you can now remotely administer your X11vnc host.

Hope this is helpful :-)

Feel free to leave comments, or questions below.

End of Line.
