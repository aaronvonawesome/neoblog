---
title: "Jellyfin + Tube Archivist = Nifty"
date: 2023-08-31T22:20:27-04:00
lastmod: 2023-09-05T21:54:00-04:00
draft: true
comment:
  enable: false
description: "**** Enter description ******"
categories:
  - Jellyfin
  - "Tube Archivist"
  - Containers
---

## TL;DR

Here is a Docker Compose file that you can use to set up [Tube Archivist](https://www.tubearchivist.com/) and use [Jellyfin](https://jellyfin.org/) to watch the downloaded content.

### docker-compose.yaml

Create a file, and name it: `docker-compose.yaml`

Then copy and paste the text below; **be sure to update the values located inside the brackets** (`[]`).

```yaml
---
# ##############################################################################################
# #                                                                                            #
# #                               Jelly + TubeArchivist Server                                 #
# #                                                                                            #
# ##############################################################################################

version: "2.1"

services:

  jellyfin:
    image: "linuxserver/jellyfin:amd64-latest"
    network_mode: host
    environment:
      PUID: "[your user's UID]"
      PGID: "[your user's GID]"
      TZ: "America/New_York"
      # optional
      JELLYFIN_PublishedServerUrl: "[your server's IP address]" # Example: "192.168.7.7"
    volumes:
      - ./library/:/config/
      - ./tvseries:/data/tvshows/
      - ./movies/:/data/movies/
    devices:
      # IMPORTANT: This is for hardware acceleration with Intel graphics
      #   Please read the "Hardware Acceleration" section here: https://hub.docker.com/r/linuxserver/jellyfin
      - "/dev/dri:/dev/dri"
    container_name: jellyfin-von-awesome
    restart: unless-stopped

  tubearchivist:
    container_name: tubearchivist-von-awesome
    restart: unless-stopped
    image: "bbilly1/tubearchivist:latest"
    ports:
      - "8000:8000"
    volumes:
      - ./tvseries/YouTube/:/youtube/
      - ./cache/:/cache/
    environment:
      ES_URL: "http://archivist-es:9200"     # needs protocol e.g. http and port
      REDIS_HOST: archivist-redis          # don't add protocol
      PUID: "[your user's UID]"
      PGID: "[your user's GID]"
      HOST_UID: "[your user's UID]"
      HOST_GID: "[your user's GID]"
      TA_HOST: "[your server's IP address]" # Example: "192.168.7.7"
      TA_USERNAME: [gandalf]                      # your initial TA credentials
      TA_PASSWORD: "[Keep it secret, keep it safe!]"          # your initial TA credentials
      ELASTIC_PASSWORD: "[Keep it secret, keep it safe!]"     # set password for Elasticsearch
      TZ: "America/New_York"                          # set your time zone
    depends_on:
      - jellyfin
      - archivist-es
      - archivist-redis

  archivist-redis:
    image: "redislabs/rejson:latest"          # for arm64 use bbilly1/rejson
    container_name: archivist-redis
    restart: always
    expose:
      - "6379"
    volumes:
      - redis:/data/
    depends_on:
      - archivist-es

  archivist-es:
    image: "bbilly1/tubearchivist-es"         # only for amd64, or use official es 7.17.3
    container_name: archivist-es
    restart: always
    environment:
      xpack.security.enabled: "true"
      ELASTIC_PASSWORD: "[Keep it secret, keep it safe!]"       # matching Elasticsearch password
      discovery.type: single-node
      ES_JAVA_OPTS: "-Xms512m -Xmx512m"
      path.repo: "/usr/share/elasticsearch/data/snapshot"
    ulimits:
      memlock:
        soft: -1
        hard: -1
    volumes:
      - es:/usr/share/elasticsearch/data/
    expose:
      - "9200"

volumes:
  redis:
  es:
```


## The Rest of the Story


## Conclusion
