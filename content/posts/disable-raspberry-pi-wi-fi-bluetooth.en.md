---
title: "Disable Raspberry Pi Wi-Fi & Bluetooth"
date: 2020-05-29T23:12:00-05:00
lastmod: 2020-05-29T23:12:00-05:00
draft: false
comment:
  enable: true
description: "How to disable the Wi-Fi and Bluetooth on your Raspberry Pi if you only want to use the Ethernet connection."
categories:
  - "Raspberry Pi"
  - "Rasberry Pi OS"
---

## TL;DR

Add the following text to the end of the `/boot/config.txt` on the MicroSD card of your Raspberry Pi:
```ini
# Disable Wifi and Bluetooth
dtoverlay=disable-wifi
dtoverlay=disable-bt
```

## The Rest of the Story

I've been setting up Raspberry Pi devices (3B+) as servers, and only really need them to be connected via the Ethernet connection.  Having the Wi-Fi and Bluetooth active seems like a waste.  A quick Internet search uncovers the answer as usual, so I just want to document this for future reference.

Thanks to [this StackExchange post](https://raspberrypi.stackexchange.com/a/62522 "Disable WiFi (wlan0) on Pi 3"); here are the details.

### Prerequisites

I'm assuming you have installed [Raspberry Pi OS (previously called Raspbian)](https://www.raspberrypi.org/documentation/installation/installing-images/README.md "Installing operating system images").

### The DL

1. Take your MircoSD card, and open it on your PC.
1. Open this file in a text editor: `/boot/config.txt`
1. Add the following text to the end of the file:
    ```ini
    # Disable Wifi and Bluetooth
    dtoverlay=disable-wifi
    dtoverlay=disable-bt
    ```
1. Now save the file
1. Safely eject your MicroSD, put it back in your Raspberry Pi
1. Power on your Raspberry Pi, and not the Wi-Fi and Bluetooth will be deactivated

## Conclusion

End of Line.
