---
title: "Convert an RPM file to a DEB file"
date: 2019-01-08T16:20:40-05:00
lastmod: 2019-01-08T16:20:40-05:00
draft: false
comment:
  enable: true
description: "Steps to convert an RPM file to a DEB file"
categories:
  - "Linux Mint"
---

## TL;DR

Convert an RPM to a DEB:
```bash
sudo alien -k --scripts my-rpm-file.rpm
```

### Source
Credit goes to my source article [found at this link](https://www.howtoforge.com/converting_rpm_to_deb_with_alien "HowtoForge Article")

## The Rest of the Story

So I needed to convert an RPM file to a DEB file.  If you have ever used [BlueJeans](https://www.bluejeans.com/downloads "BlueJeans downloads") for conferences calls, the desktop App does work on Linux, but is only available as an RPM file.  This is unfortunate for me as I use Linux Mint.  A little bit of searching, and here's how it's done.

### Prerequisite

You'll need to have `alien` installed:

```bash
sudo apt install alien
```

### Steps

Once `alien` is installed, you can convert an RPM file to a DEB file, and install it:

Step 1:
```bash
sudo alien -k --scripts my-rpm-file.rpm
```

This will create the file `my-deb-file.deb`, and to install the DEB file move to Step 2.

Step 2:

```bash
sudo dpkg -i my-deb-file.deb
```

That's it - hope this is helpful.

End of Line.
