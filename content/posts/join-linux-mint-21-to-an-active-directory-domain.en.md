---
title: "Join Linux Mint 21 to an Active Directory Domain"
date: 2023-01-17T15:01:08-05:00
draft: true
comment:
  enable: false
description: "Here are the steps to join your Linux Mint 21 laptop connected to an Active Directory Domain."
categories:
  - "Linux Mint"
  - "Active Directory"
slug: join-linux-mint-21-to-an-active-directory-domain
---

## TL;DR

Here are all the steps needed to add your Linux Mint 21 computer to a Windows Active Directory Domain.  For more detail, and explanation, please read [The Rest of the Story](#the-rest-of-the-story).

```bash
sudo apt install realmd sssd libnss-sss libpam-sss samba-libs packagekit-tools
```

Restart your computer, then edit `krb5.conf`.

```bash
sudo xed /etc/krb5.conf
```

```ini
[libdefaults]
    udp_preference_limit = 0
    default_realm = AWESOME.COM
```

```bash
sudo pam-auth-update
```

Join the Active Directory Domain.

```bash
sudo realm join --verbose --user=aawesome-a --computer-ou=OU=Computers,OU="ASM Headquarters",DC=awesome,DC=com awesome.com
```

Edit `sssd.conf`.
```bash
sudo xed /etc/sssd/sssd.conf
```

```ini
[sssd]
domains = awesome.com
config_file_version = 2
services = nss, pam

[domain/awesome.com]
default_shell = /bin/bash
krb5_store_password_if_offline = True
cache_credentials = True
krb5_realm = AWESOME.COM
realmd_tags = manages-system joined-with-adcli
id_provider = ad
ldap_sasl_authid = [[computer_name]]$
fallback_homedir = /home/%u@%d
ad_domain = awesome.com
use_fully_qualified_names = True
ldap_id_mapping = True
access_provider = ad
```

Restart the SSSD Service.

```bash
sudo systemctl restart sssd.service
```

Finally, [update your login screen](#step-7-modify-your-login-window).

Restart your computer, and you should able to log in using your Active Directory Credentials.

**Update 8/30/2019:**  [Click here](#update-8302019) to see some optional, final steps you could take.

## The Rest of the Story


#### Step 0: Install the Needed Packages

Open up your terminal, and enter the text below to get the needed packages installed.

```bash
sudo apt install realmd sssd libnss-sss libpam-sss samba-libs packagekit-tools
```

After I installed the packages, I went ahead and restarted my machine.

#### Step 1: Edit Your krb5.conf File

Start by opening `krb5.conf`:

```bash
sudo xed /etc/krb5.conf
```

You can replace the contents of the current file with the text below.

***Note:*** *Values inside the double brackets (i.e. - [[value]]), need to be replaced with the correct values for your environment.  Replace the value, and additionally, remove the double brackets. (i.e. - domain = [[my-domain.com]] --> domain = awesome.com)*

```ini
[libdefaults]
    udp_preference_limit = 0
    default_realm = [[YOUR-REALM.COM]]  #YOUR-REALM.COM should be in CAPS
```

To finish up this step, run:

```bash
sudo pam-auth-update
```

#### Step 4: Test Your Credentials

Even though your computer may not be bound to the Active Directory yet, you can now test your login credentials to make sure everything is set up correctly so far.

Run the command below.

```bash
realm discover [[my-domain.com]]
```

Successful results should look similar to the output below.

```bash
my-domain.com
  type: kerberos
  realm-name: MY-REALM.COM
  domain-name: my-domain.com
  configured: kerberos-member
  server-software: active-directory
  client-software: sssd
  required-package: sssd-tools
  required-package: sssd
  required-package: libnss-sss
  required-package: libpam-sss
  required-package: adcli
  required-package: samba-common-bin
  login-formats: %U@my-domain.com
  login-policy: allow-realm-logins
```

#### Step 5: Join the Active Directory Domain

Time to join your Active Directory.  You'll need a Network Administrator, or someone with a Network Admin username/password in order to get your computer joined to the Active Directory realm.

Enter the text below into your terminal, and don't forget to replace the values in the double brackets (along with the brackets).

```bash
sudo realm join --verbose --user=[[network-admin-username]] --computer-ou=OU=[[Computers]],OU="[[Active-Directory-OU-Value]]",DC=[[my-domain-without-the-com]],DC=com [[my-domain.com]]
```

You'll enter the Network Administrator username, and you'll be prompted for the password.

```bash
 * Resolving: _ldap._tcp.my-domain.com
 * Performing LDAP DSE lookup on: 10.1.1.14
 * Performing LDAP DSE lookup on: 10.1.1.15
 * Performing LDAP DSE lookup on: 10.1.1.28
 * Successfully discovered: my-domain.com
Password for awesome-admin:
```

After the Network Administrator password has been entered, the rest of the output should look similar to this:

```bash
 * Unconditionally checking packages
 * Resolving required packages
 * LANG=C /usr/sbin/adcli join --verbose --domain my-domain.com --domain-realm MY-REALM.COM --domain-controller 10.1.1.14 --computer-ou OU=Computers,OU=SHORT-DOMAIN-NAME Headquarters,DC=domain-controller,DC=com --os-name Linux Mint --os-version 19.1 --login-type user --login-user network-admin-username --stdin-password --user-principal
 * Using domain name: my-domain.com
 * Calculated computer account name from fqdn: COMPUTER-NAME
 * Using domain realm: my-domain.com
 * Sending netlogon pings to domain controller: cldap://10.1.1.14
 * Received NetLogon info from: SERVER.my-domain.com
 * Wrote out krb5.conf snippet to /var/cache/realmd/adcli-xyzab-oFOwIT/krb5.d/adcli-krb5-conf-wCGqIO
 * Authenticated as user: network-admin-username@MY-REALM.COM
 * Looked up short domain name: SHORT-DOMAIN-NAME
 * Using fully qualified name: computer-name
 * Using domain name: my-domain.com
 * Using computer account name: COMPUTER-NAME
 * Using domain realm: my-domain.com
 * Calculated computer account name from fqdn: COMPUTER-NAME
 * With user principal: host/computer-name@MY-REALM.COM
 * Generated 120 character computer password
 * Using keytab: FILE:/etc/krb5.keytab
 * Found computer account for COMPUTER-NAME$ at: CN=COMPUTER-NAME,OU=Computers,OU=SHORT-DOMAIN-NAME Headquarters,DC=my-domain,DC=com
 * Set computer password
 * Retrieved kvno '7' for computer account in directory: CN=COMPUTER-NAME,OU=Computers,OU=SHORT-DOMAIN-NAME Headquarters,DC=my-domain,DC=com
 * Modifying computer account: userAccountControl
 * Modifying computer account: operatingSystemVersion, operatingSystemServicePack
 * Modifying computer account: userPrincipalName
 * Discovered which keytab salt to use
 * Added the entries to the keytab: COMPUTER-NAME$@MY-REALM.COM: FILE:/etc/krb5.keytab
 * Added the entries to the keytab: host/COMPUTER-NAME@MY-REALM.COM: FILE:/etc/krb5.keytab
 * Added the entries to the keytab: host/COMPUTER-NAME@MY-REALM.COM: FILE:/etc/krb5.keytab
 * Cleared old entries from keytab: FILE:/etc/krb5.keytab
 * Added the entries to the keytab: host/computer-name@MY-REALM.COM: FILE:/etc/krb5.keytab
 * Added the entries to the keytab: RestrictedKrbHost/COMPUTER-NAME@MY-REALM.COM: FILE:/etc/krb5.keytab
 * Added the entries to the keytab: RestrictedKrbHost/computer-name@MY-REALM.COM: FILE:/etc/krb5.keytab
 * /usr/sbin/update-rc.d sssd enable
 * /usr/sbin/service sssd restart
 * Successfully enrolled machine in realm
```

#### Step 6: Edit Your sssd.conf File

Open up `sssd.conf` for editing

```bash
sudo xed /etc/sssd/sssd.conf
```

Copy and paste the text below into the file.  Again, don't forget to replace the values in the double brackets (along with the brackets).

```ini
[sssd]
domains = awesome.com
config_file_version = 2
services = nss, pam

[domain/awesome.com]
default_shell = /bin/bash
krb5_store_password_if_offline = True
cache_credentials = True
krb5_realm = AWESOME.COM
realmd_tags = manages-system joined-with-adcli
id_provider = ad
ldap_sasl_authid = [[computer_name]]$
fallback_homedir = /home/%u@%d
ad_domain = awesome.com
use_fully_qualified_names = True
ldap_id_mapping = True
access_provider = ad
```

Save the file, then close the text editor, and run the command below.

```bash
sudo systemctl restart sssd.service
```

#### Step 7: Modify Your Login Window

And now for our last step.  This is specific for Linux Mint.

Start by opening the Login Window settings, as seen in the screenshot below.

![Login Window 1](/posts/join-linux-mint-19-to-an-active-directory-domain/login-window-01.png "Open Login Window")

Now make sure you settings look like this:

![Login Window 2](/posts/join-linux-mint-19-to-an-active-directory-domain/login-window-02.png "Login Window Settings")

## Conclusion

Those are the steps.  If anything is unclear, let me know in the comments below.  Hope this is helpful :-)

## Update 8/30/2019

So this is an optional step that may not have to happen in every scenario.  Upon logging in with your Active Directory User, you may discover that you do not have the rights to do anything.  So, you will have to log in with a *local* administrator account, and add your *domain* account to the following groups by running these commands:
```bash
sudo usermod -a -G adm aaronvon@awesome.com
sudo usermod -a -G cdrom aaronvon@awesome.com
sudo usermod -a -G dip aaronvon@awesome.com
sudo usermod -a -G lpadmin aaronvon@awesome.com
sudo usermod -a -G plugdev aaronvon@awesome.com
sudo usermod -a -G sambashare aaronvon@awesome.com
sudo usermod -a -G sudo aaronvon@awesome.com
```

End of Line.
