---
title: "System Limit for Number of File Watchers Reached"
date: 2019-03-13T20:13:36-05:00
lastmod: 2019-03-13T20:13:36-05:00
draft: false
comment:
  enable: true
description: "If you receive the following error: 'Error: ENOSPC: System limit for number of file watchers reached, watch...', I'll go over a possible solution."
categories:
  - "Vue.js"
---

## TL;DR

Using the [Jest](https://jestjs.io/ "Jestjs.io") plugin in [Vue.js](https://wwww.vuejs.org "Vuejs.org") in Linux:
```bash
npx vue-cli-service test:unit --watch
```

And you receive this error on your Linux PC:
> Error: ENOSPC: System limit for number of file watchers reached, watch...

Open up Terminal, and to resolve the error, run:

```bash
echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p
```

### Source

- https://github.com/facebook/jest/issues/3254#issuecomment-297214395

## The Rest of the Story

I'll be the first to admit that I have way too way interests, and not enough time.  How this relates, is that now I am going through some [Vue.js](https://wwww.vuejs.org "Vuejs.org") tutorials.

After running the following command while learning about the [Jest](https://jestjs.io/ "Jestjs.io") plugin in [Vue.js](https://wwww.vuejs.org "Vuejs.org"):
```bash
npx vue-cli-service test:unit --watch
```

I received this error:
> Error: ENOSPC: System limit for number of file watchers reached, watch...

Apparently, this is a Linux issue.  Currently I'm running [Linux Mint 19.1](https://www.linuxmint.com/ "Linux Mint").

Run the following command in the Terminal to resolve the issue:

```bash
echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p
```

### Source

And here is where I discovered the solution.  Big thanks to the original poster!

Original post: https://github.com/facebook/jest/issues/3254#issuecomment-297214395

Hope this helps someone else find this solution quicker :-)

End of Line.
