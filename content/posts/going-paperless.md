---
title: "Going Paperless"
date: 2021-12-30T00:26:00-05:00
lastmod: 2021-12-30T00:26:00-05:00
draft: false
comment:
  enable: true
description: "The outcome vision for this post is to provide an example Docker Compose file that you can use, and the setup steps to help you get your very own Paperless-ng server running.  You then will be able to tweak the files to your liking.  The documentation for Paperless-ng is great, however, I hope to distill it down even further in order jump start your Paperless-ng sever setup."
categories:
  - Containers
  - Paperless
  - Self-hosted
---

## TL;DR

### Paperless-ng

Here is how you can set up a Paperless-ng Server with Docker + Docker Compose.

1. [Install Docker + Docker Compose](#docker-installation)
2. Create a folder where all the Paperless-ng files and folders can live:
    ```bash
    mkdir -p ~/Docker/paperless-ng
    ```
3. Now create the `docker-compose.env` and `docker-compose.yaml` files in that directory:
    ```bash
    touch ~/Docker/paperless-ng/docker-compose.env
    ```
    ```bash
    touch ~/Docker/paperless-ng/docker-compose.yaml
    ```
4. Copy the contents below into the `docker-compose.env` file (expand code block below to copy):
{{< code file="static/posts/going-paperless/docker-compose.example.env" language="yaml" >}}
*Gitlab: [docker-compose.example.env](https://gitlab.com/aaronvonawesome/neoblog/-/blob/master/static/posts/going-paperless/docker-compose.example.env)*
5. Copy the contents below into the `docker-compose.yaml` file:
{{< code file="static/posts/going-paperless/docker-compose.example.yaml" language="yaml" >}}
*Gitlab: [docker-compose.example.yaml](https://gitlab.com/aaronvonawesome/neoblog/-/blob/master/static/posts/going-paperless/docker-compose.example.yaml)*
6. Now open up your terminal again:
    ```bash
    cd ~/Docker/paperless-ng
    ```
    ```bash
    docker-compose up -d
    ```
7. Open up your favorite (Firefox 😉️) browser, and navigate to `http://localhost:8000`, and use the username `admin` and password `admin` to log in for the first time.

### Paperless-ng + FTP

Here is how you can set up a Paperless-ng + FTP Server with Docker + Docker Compose.

1. [Install Docker + Docker Compose](#docker-installation)
2. Create a folder where all the Paperless-ng files and folders can live:
    ```bash
    mkdir -p ~/Docker/paperless-ng
    ```
3. Now create the `docker-compose.env` and `docker-compose.yaml` files in that directory:
    ```bash
    touch ~/Docker/paperless-ng/docker-compose.env
    ```
    ```bash
    touch ~/Docker/paperless-ng/docker-compose.yaml
    ```
4. Copy the contents below into the `docker-compose.env` file (expand code block below to copy):
{{< code file="static/posts/going-paperless/docker-compose.example.env" language="yaml" >}}
*Gitlab: [docker-compose.example.env](https://gitlab.com/aaronvonawesome/neoblog/-/blob/master/static/posts/going-paperless/docker-compose.example.env)*
5. Copy the contents below into the `docker-compose.yaml` file:

    **Important:** Make sure you change the line `PUBLICHOST: "192.168.10.7"` to use **your computer's IP Address**

    {{< code file="static/posts/going-paperless/docker-compose.ftp-example.yaml" language="yaml" >}}
    *Gitlab: [docker-compose.ftp-example.yaml](https://gitlab.com/aaronvonawesome/neoblog/-/blob/master/static/posts/going-paperless/docker-compose.ftp-example.yaml)*

6. Now open up your terminal again:
    ```bash
    cd ~/Docker/paperless-ng
    ```
    ```bash
    docker-compose up -d
    ```

You can now scan documents directly from your network printer, and Paperless-ng will put up the file, process it, and add it to you document library.

## The Rest of the Story

### Background

So it's like this.  I was looking for a way archive mail (snail mail) that I was receiving, and stumbled upon [The Paperless Project](https://github.com/the-paperless-project/paperless).  That was two years ago, and Paperless has been running solid ever since.

More recently I was checking for updates, and discovered that the original Developer of [The Paperless Project](https://github.com/the-paperless-project/paperless) had stepped down, and now the current project is called [Paperless-ng](https://github.com/jonaswinkler/paperless-ng) (ng - next generation?).  I was very pleased to see the continuation of this project!

And the icing on the cake, the [Linuxserver.io Team](https://www.linuxserver.io/) picked up the Paperless-ng project, and are [providing their own](https://hub.docker.com/r/linuxserver/paperless-ng) Open Container Initiative (OCI) image for the Paperless-ng project.  If ever I am wanting to run an OCI container application, either with [Docker](https://hub.docker.com/) or [Podman](https://podman.io/), I always check [fleet.linuxserver.io](https://fleet.linuxserver.io/), and highly recommend you check out the other images they offer! :-)

### Outcome Vision

The outcome vision for this post is to provide an example [Docker Compose](https://docs.docker.com/compose/) file that you can use, and the setup steps to help you get your very own Paperless-ng server running.  You then will be able to tweak the files to your liking.  The [documentation for Paperless-ng](https://paperless-ng.readthedocs.io/en/latest/index.html) is great, however, I hope to distill it down even further in order jump start your Paperless-ng sever setup.

### Prerequisites

Paperless-ng [can be installed multiple ways](https://paperless-ng.readthedocs.io/en/latest/setup.html#installation), but I'm going to focus on the OCI image setup using Docker.

#### Docker Installation

So hopefully it's obvious that you'll need to install Docker and Docker Compose on your [Linux Mint](https://www.linuxmint.com) (or other Ubuntu-based) machine.

1. Open up a terminal, and type the following:
    ```bash
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
    ```
2. Install the Docker repository
    ```bash
    echo \
      "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
      focal stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
    ```
    **Note:** You'll notice `focal stable` above, you can change that to `$(lsb_release -cs) stable` if you are using vanilla Ubuntu.
3. Install Docker, and Docker Compose
    ```bash
    sudo apt update
    sudo apt install docker-ce docker-ce-cli containerd.io docker-compose
    ```
4. Add your Linux User to the `docker` group so you do not have to type `sudo` when using Docker commands
    ```bash
    sudo groupadd docker
    sudo usermod -aG docker $USER
    ```
5. Log out, and log back in to apply the group changes to your Linux User account.

*Source: https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository*

### Starting up Your Paperless Server

Thanks to the magic of containerization, getting start is quite simple.

Do me a favor, and set things up exactly as I show below.  Then when you see things working tweak the heck out of it. 😇️

1. Open up your terminal, and let's create a home where all the Paperless-ng files and folders can live:
    ```bash
    mkdir -p ~/Docker/paperless-ng
    ```
2. Now create the `docker-compose.env` and `docker-compose.yaml` files in that directory:
    ```bash
    touch ~/Docker/paperless-ng/docker-compose.env
    ```
    ```bash
    touch ~/Docker/paperless-ng/docker-compose.yaml
    ```
3. Copy the contents below into the `docker-compose.env` file (expand code block below to copy):
{{< code file="static/posts/going-paperless/docker-compose.example.env" language="yaml" >}}
*Gitlab: [docker-compose.example.env](https://gitlab.com/aaronvonawesome/neoblog/-/blob/master/static/posts/going-paperless/docker-compose.example.env)*
4. Copy the contents below into the `docker-compose.yaml` file:
{{< code file="static/posts/going-paperless/docker-compose.example.yaml" language="yaml" >}}
*Gitlab: [docker-compose.example.yaml](https://gitlab.com/aaronvonawesome/neoblog/-/blob/master/static/posts/going-paperless/docker-compose.example.yaml)*
5. Now open up your terminal again:
    ```bash
    cd ~/Docker/paperless-ng
    ```
    ```bash
    docker-compose up -d
    ```
6. Monitor the initialization process:
    ```bash
    docker-compose logs -f
    ```
    In the output, you should see:
    ```bash
    ...
    paperless-vonawesomeweb | [2021-12-29 00:50:41 -0500] [365] [INFO] Server is ready. Spawning workers
    paperless-vonawesomeweb | [2021-12-29 00:50:41,917] [INFO] [paperless.management.consumer] Using inotify to watch directory for changes: /data/consume
    ...
    paperless-vonawesomeweb | [2021-12-29 00:51:12,451] [INFO] [paperless.sanity_checker] Sanity checker detected no issues.
    ...
    ```
7. Press `Ctrl+c` to exit from the "docker-compose logs" output
8. Open up your favorite (Firefox 😉️) browser, and navigate to `http://localhost:8000`, and use the username `admin` and password `admin` to log in for the first time.

    Upon a successful login, you'll be greeted with the Paperless-ng Dashboard:

    ![Paperless Dashboard](/posts/going-paperless/01-paperless-dashboard.png "Paperless-ng Dashboard")

9. If you want to change your password, you can do that in the **admin** section:

    ![Admin Section](/posts/going-paperless/02-admin.png "Admin Section")

### Your First Upload

Ok, you've logged in, you're at the Dashboard.  Now try uploading a file.

The end result is that the file is "processed", and added to your document library.  Over time you'll add more and more documents, and you'll be able to find your specific document amongst the haystack of all the others with a simple text search thanks to the Optical Character Recognition (OCR) built into Paperless-ng.

Let the uploading begin!

1. On the Dashboard, either drag and drop the file you would like to upload, or click **Browse files** under the **Upload new documents** section:

    ![Upload File](/posts/going-paperless/03-upload-a-file.png "Upload a File")

2. Wait for your file to upload

    ![Process File](/posts/going-paperless/04-upload-a-file.png "File Processing...")

    ![Upload Finished](/posts/going-paperless/05-upload-a-file.png "Upload Finished")

3. And that's it!  Document uploaded/processed.  You can click on **Documents** in the menu on the right side of the dashboard to view, edit, or search your documents:

    ![View Documents](/posts/going-paperless/06-view-documents.png "View Documents")

    ![Document Library](/posts/going-paperless/07-view-documents.png "Document Library")

### Bonus: Upload via FTP

So instead of uploading everything via the web interface, how would you like to go directly from your scanner to paperless?

Well, you're in luck, there's a Docker Compose for that! 🤓️

#### Prerequisites

1. You'll need a printer that can send files via File Transfer Protocol (FTP), and your printer will need to be connected to your Local Area Network (LAN).
2. Also, make a note of your computer's IP Address, you need it later.

#### Starting up Your Paperless Server with FTP

1. Complete steps 1 - 3 in the ***Starting up Your Paperless Server*** [section above](#starting-up-your-paperless-server), then move on to the next step below.
2. Copy the contents below into the `docker-compose.yaml` file:

    **Important:** Make sure you change the line `PUBLICHOST: "192.168.10.7"` to use **your computer's IP Address**

    {{< code file="static/posts/going-paperless/docker-compose.ftp-example.yaml" language="yaml" >}}
    *Gitlab: [docker-compose.ftp-example.yaml](https://gitlab.com/aaronvonawesome/neoblog/-/blob/master/static/posts/going-paperless/docker-compose.ftp-example.yaml)*

3. Now open up your terminal again:
    ```bash
    cd ~/Docker/paperless-ng
    ```
    ```bash
    docker-compose up -d
    ```
4. Monitor the initialization process:

    ```bash
    docker-compose logs -f
    ```

    Again, you'll look for output similar to below:

    ```bash
    ...
    paperless-vonawesomeweb | [2021-12-29 00:50:41 -0500] [365] [INFO] Server is ready. Spawning workers
    paperless-vonawesomeweb | [2021-12-29 00:50:41,917] [INFO] [paperless.management.consumer] Using inotify to watch directory for changes: /data/consume
    ...
    paperless-vonawesomeweb | [2021-12-29 00:51:12,451] [INFO] [paperless.sanity_checker] Sanity checker detected no issues.
    ...
    ```

    Additionally, make sure the FTP Server has started up.  You should see output similar to this:

    ```bash
    paperless-vonawesomepureftpd | Creating user...
    paperless-vonawesomepureftpd | Password:
    paperless-vonawesomepureftpd | Enter it again:
    paperless-vonawesomepureftpd |  root user give /home/paperless/ftp-files directory 1001 owner
    paperless-vonawesomepureftpd | Setting default port range to: 30000:30009
    paperless-vonawesomepureftpd | Setting default max clients to: 5
    paperless-vonawesomepureftpd | Setting default max connections per ip to: 5
    paperless-vonawesomepureftpd | Starting Pure-FTPd:
    paperless-vonawesomepureftpd |   pure-ftpd  -l puredb:/etc/pure-ftpd/pureftpd.pdb -E -j -R -P 192.168.10.7   -p 30000:30009 -c 5 -C 5
    ```

5. Press `Ctrl+c` to exit from the "docker-compose logs" output.

You can now scan documents directly from your network printer, and Paperless-ng will put up the file, process it, and add it to you document library.

## Conclusion

There is so much more you can do to your documents, like [tagging or labeling documents automatically](https://paperless-ng.readthedocs.io/en/latest/advanced_usage.html#matching-tags-correspondents-and-document-types) to help you better organize your documents.

However, this post is just meant to get you setup and started.  [The documentation for Paperless-ng](https://paperless-ng.readthedocs.io/en/latest/index.html) is phenomenal, so if you want to dig deeper, check that out.

As always, questions or comments welcome.

I hope you find this helpful! :-)


End of Line.
