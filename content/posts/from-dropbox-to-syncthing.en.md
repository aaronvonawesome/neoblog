---
title: "From Dropbox to Syncthing"
date: 2018-09-04T14:45:00-05:00
lastmod: 2018-09-04T14:45:00-05:00
draft: false
comment:
  enable: true
description: "If you're looking for a Dropbox replacement due to Dropbox nixing support for your file system.  Consider giving Syncthing a shot."
categories:
  - "Cloud Sync"
  - Self-hosted
---

## TL;DR

If you're looking for a Dropbox replacement due to Dropbox nixing support for your file system.  Consider giving [Syncthing](https://syncthing.net/ "Syncthing.net") a shot.

1. Click here for [Syncthing installation instructions](https://docs.syncthing.net/intro/getting-started.html#installing "Syncthing installation instructions") (or see my abbreviated instructions below).
   - You could also check out this [installation video](https://www.youtube.com/watch?v=foTxCfhxVLE "Syncthing on Linux Mint / Ubuntu / Debian with PPA Sources")
1. Don't forget to [enable Syncthing on your firewall](https://docs.syncthing.net/users/firewall.html?highlight=firewall#uncomplicated-firewall-ufw "Allow Syncthing through firewall").
1. For more information, check on the [superb Syncthing documentation](https://docs.syncthing.net/users/faq.html "Syncthing FAQ").

## The Rest of the Story

I recently received this message after logging into my PC:
<br />
<br />
![Dropbox Warning](/posts/from-dropbox-to-syncthing/Dropbox-Warning.png "Move Dropbox location Dropbox will stop syncing in November")

I didn't really pay much attention to it, but it kept popping up, and I run two instances of Dropbox, so it kept popping up quite a bit.

Finally I decided to look into it, and I discovered this [Dropbox forum post](https://www.dropboxforum.com/t5/Syncing-and-uploads/Dropbox-client-warns-me-that-it-ll-stop-syncing-in-Nov-why/m-p/290065#M42255 "Dropbox forum post").

I run Linux Mint, so Ubuntu at the core, and I run with an encrypted home directory, so ecryptfs, which is what is prompting the message from Dropbox.  Starting in November, Dropbox will only support the following file systems: NTFS for Windows, HFS+ or APFS for Mac, and Ext4 for Linux. To be fair, if you use Ext4 for Linux with full disk encryption, you would be fine.  However, I still run an encrypted home folder even with full-disk encryption.  Moreover, on something like my older laptop, I don't really want the overhead of full-disk encryption; home folder encryption works extremely well.

So, I did a bit of searching online for some Dropbox alternatives, and ran across a tool called [Syncthing](https://docs.syncthing.net/users/faq.html "Syncthing FAQ"). This cloud syncing tool doesn't store any files on a central server, but instead connects your machines directly to one another, wherever they may be, with an encrypted connection.  I had to read through the [documentation](https://docs.syncthing.net/specs/ "Syncthing Specification Documentation") to really get comfortable with the idea, but I'm there now.

The setup is quite straight forward, and there is even a GUI/notification app that will reside in your taskbar.  For Linux it is [Syncthing-GTK](https://github.com/syncthing/syncthing-gtk "Syncthing-GTK GitHub page"), and for Windows it is [SyncTrayzor](https://github.com/canton7/SyncTrayzor "SyncTrayzor GitHub page").  There is also an Android app available on the [Google Play Store](https://play.google.com/store/apps/details?id=com.nutomic.syncthingandroid "Syncthing on Google Play"), and in the [F-Droid Repository](https://f-droid.org/packages/com.nutomic.syncthingandroid/ "Syncthing of F-Droid"), however I have not experimented with those enough to comment.

Here are the steps to get Syncthing set up for Linux Mint 18/19 (or Ubuntu):

1. Install the [Syncthing PPA](https://apt.syncthing.net/ "Debian/Ubuntu Packages")
```bash
# Add the release PGP keys:
curl -s https://syncthing.net/release-key.txt | sudo apt-key add -

# Add the "stable" channel to your APT sources:
echo "deb https://apt.syncthing.net/ syncthing stable" | sudo tee /etc/apt/sources.list.d/syncthing.list

# Update and install syncthing:
sudo apt-get update
sudo apt-get install syncthing
```

2. Install the Syncthing-GTK
  - [Download the lastest DEB file](https://download.opensuse.org/repositories/home:/kozec/xUbuntu_18.04/amd64/ "Syncthing-GTK DEB installer"), and double-click to install it.
    - As of the time of this writing, this in the latest installer is: [syncthing-gtk_0.9.4_amd64.deb](https://download.opensuse.org/repositories/home:/kozec/xUbuntu_18.04/amd64/syncthing-gtk_0.9.4_amd64.deb "Latest Syncthing-GTK installer")

3. If you use ufw (or gufw), you'll need to [allow Syncthing](https://docs.syncthing.net/users/firewall.html?highlight=firewall#uncomplicated-firewall-ufw "Allow Syncthing through firewall")
```bash
sudo ufw allow syncthing
```

And that's all for the install. If you have any issues with my instructions, you can follow the [instructions from the Syncthing website](https://docs.syncthing.net/intro/getting-started.html#installing "Syncthing installation instructions").  Additionally, you can check out these installation instruction videos:
  - [Linux Installation Video](https://www.youtube.com/watch?v=foTxCfhxVLE "Syncthing on Linux Mint / Ubuntu / Debian with PPA Sources")
  - [Windows Installation Video](https://www.youtube.com/watch?v=2QcO8ikxzxA "Syncthing on Windows")

One final thing I do after installation; I turn off the option for "NAT traversal". Here is how you go about turning that option off (after installing Syncthing-GTK):
<br />
<br />
1. Right-click on the taskbar icon, and select "Open Web Interface"
<br />
<br />
![Open Web Interface 1](/posts/from-dropbox-to-syncthing/Open-Web-Interface-01.png "Open Web Interface from taskbar")

2. Click on "Actions" in the upper, right-hand corner.  Then click on "Settings"
<br />
<br />
![Open Web Interface 2](/posts/from-dropbox-to-syncthing/Open-Web-Interface-02.png "Open Settings")

3. Once the Settings dialog appears, click on the "Connections" tab, uncheck the "Enable NAT traversal" option, and click "Save"
<br />
<br />
![Open Web Interface 3](/posts/from-dropbox-to-syncthing/Open-Web-Interface-03.png "Disable NAT traversal")

I prefer that my applications NOT try to bypass my firewall.  However, if I am understanding that incorrectly, please feel free to correct me in the comments below.

That's it for this installment. If you are running into issues due to Dropbox nixing your file system support, consider [Syncthing](https://syncthing.net/ "Syncthing.net").  If you would like more detail, you can refer to their [detailed documentation](https://docs.syncthing.net/users/faq.html "Syncthing FAQ").

End of Line.
