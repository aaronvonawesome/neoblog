---
title: "Enter the Hugo Multiverse"
date: 2022-06-12T23:21:09-04:00
lastmod: 2023-08-31T22:12:00-04:00
draft: false
comment:
  enable: true
description: "This is how you can install multiple version of Hugo using the Hugo Ubuntu Snap Package"
categories:
  - Hugo
  - "Snap Packages"
---

## TL;DR

If you want to run multiple versions of Hugo, enter these commands in your terminal:

```bash
sudo snap set system experimental.parallel-instances=true

sudo snap install hugo hugo_extended

sudo snap refresh hugo_extended --channel=extended/stable
````

You can now run the [Hugo Extended Version](https://gohugo.io/troubleshooting/faq/#i-get--this-feature-is-not-available-in-your-current-hugo-version) by running something like this in your terminal whilst in the directory of your Hugo project:

```bash
hugo_extended serve -D --noHTTPCache
```

### Source
Steps used where found [at this source](https://snapcraft.io/docs/parallel-installs).


## The Rest of the Story

There are certain Hugo themes (like the one for this website) that need the [Hugo Extended Version](https://gohugo.io/troubleshooting/faq/#i-get--this-feature-is-not-available-in-your-current-hugo-version) in order to build.  However, certain themes may not build with the extended version of Hugo.

So, to address this, why not install both?

I have found the [Hugo snap package](https://snapcraft.io/hugo) to be perfect for building my Hugo static sites.

You can actually install multiple versions of any snap, but for this post, I am only dealing with Hugo.

Start by enabling the parallel instances Snap feature:

```bash
sudo snap set system experimental.parallel-instances=true
```

Then install your first copy of the [Hugo snap package](https://snapcraft.io/hugo):

```bash
sudo snap install hugo
```

Now install a second copy of [Hugo snap package](https://snapcraft.io/hugo), but this time you will be giving the second copy an alias of `hugo_extended`:

```bash
sudo snap install hugo hugo_extended
```

You will then change the Hugo snap package channel for the second copy of Hugo that is installed on your system:

![Hugo extended version Snap channel](/posts/hugo-multiverse/hugo-multiverse-01.png)

In the terminal, type:

```bash
sudo snap refresh hugo_extended --channel=extended/stable
```

## Conclusion

Now you can `cd` into your Hugo static site directory and build your static site using the [Hugo Extended Version](https://gohugo.io/troubleshooting/faq/#i-get--this-feature-is-not-available-in-your-current-hugo-version) with a command like this:

```bash
hugo_extended serve -D --noHTTPCache
```

End of Line.
