---
title: "How to Merge Multiple MP4 Files"
date: 2021-01-04T16:34:32-05:00
draft: false
comment:
  enable: true
description: "Here is a concise guide on how to merge multiple mp4 files into a single file."
categories:
  - "Video Editing"
---

## TL;DR

1. Install `ffmpeg`:
    ```bash
    sudo apt update
    sudo apt install ffmpeg
    ```
2. Merges files:
    ```bash
    cd Video-File-Folder
    ```
    ```bash
    ls | while read FILENAME; do echo file \'$FILENAME\'; done | ffmpeg -f concat -safe 0 -protocol_whitelist "file,pipe" -i - -codec copy merged-videos-file.mp4
    ```

### Sources
- https://superuser.com/questions/607383/concat-two-mp4-files-with-ffmpeg-without-losing-quality
- https://stackoverflow.com/questions/43333542/what-is-video-timescale-timebase-or-timestamp-in-ffmpeg
- https://www.shellhacks.com/bash-read-file-line-by-line-while-read-line-loop/

## The Rest of the Story

This might not be the most exciting first post for 2021, but one must start somewhere 😇️

So, you might have some home surveillance video cameras, for example, and those devices tend to break apart the footage into smaller files.  I've seen some break the footage apart into a file (`.mp4`) per minute of footage.  That means 60 minutes, 60 files.

I find it a bit easier to navigate through one file as opposed to sixty.

Here is how you can use `ffmpeg` on Linux Mint 20 (or other Debian/Ubuntu-based distribution) to merge all those pesky smaller `.mp4` files in a single file.

### Steps

#### Install FFMPEG
You'll need to have `ffmpeg` installed on you machine, and you can do that with the following command:
```bash
sudo apt update
sudo apt install ffmpeg
```

#### Merge All the Files!
***Note:** One thing to be aware of, is that your input files must have the same [stream types](https://stackoverflow.com/questions/43333542/what-is-video-timescale-timebase-or-timestamp-in-ffmpeg) (same formats, timebase, timescale, etc.).  If all of the inputs files come from the same device (source), you should be ok.*

1.  Start by putting all the files you want to merge into a folder.  In the following example the folder I'll be using will be `Video-File-Folder`.
1.  Make sure you have changed directory into the folder with all the files
    ```bash
    cd Video-File-Folder
    ```
1.  Run the following command in you terminal:
    ```bash
    ls | while read FILENAME; do echo file \'$FILENAME\'; done | ffmpeg -f concat -safe 0 -protocol_whitelist "file,pipe" -i - -codec copy merged-videos-file.mp4
    ```
1.  The output won't look to pretty, but when it's done, you should see a new file in the `Video-File-Folder` folder called `merged-videos-file.mp4`

## Conclusion

Multiple `.mp4` files, should now be one magnificent `.mp4` file!

End of Line.
