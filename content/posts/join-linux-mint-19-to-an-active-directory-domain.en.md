---
title: "Join Linux Mint 19/20 to an Active Directory Domain"
date: 2019-01-10T23:09:28-05:00
lastmod: 2021-07-20T15:30:00-05:00
draft: false
comment:
  enable: true
description: "Here are the steps to join your Linux Mint (or Ubuntu-based) laptop connected to an Active Directory Domain."
categories:
  - "Linux Mint"
  - "Active Directory"
slug: join-linux-mint-19-to-an-active-directory-domain
---

## TL;DR

Here are all the steps needed to add your Linux Mint computer to a Windows Active Directory Domain.  For more detail, and explanation, please read [The Rest of the Story](#the-rest-of-the-story).

```bash
sudo apt install realmd sssd sssd-tools libnss-sss libpam-sss krb5-user adcli samba-common-bin oddjob oddjob-mkhomedir packagekit samba python-dnspython
```

Restart your computer, then edit `krb5.conf`.

```bash
sudo xed /etc/krb5.conf
```

```ini
[libdefaults]
    default_realm = AWESOME.COM
    dns_lookup_kdc = true
    dns_lookup_realm = true

[realms]
    AWESOME.COM = {
        kdc = awesome.com
        admin_server = awesome.com
        master_kdc = awesome.com
        default_domain = awesome.com
        }

[domain_realm]
    .awesome.com = AWESOME.COM
    awesome.com = AWESOME.COM

[logging]
    kdc = SYSLOG:INFO
    admin_server = FILE=/var/kadm5.log
```

```bash
sudo pam-auth-update
```

Edit `realmd.conf`.

```bash
sudo xed /etc/realmd.conf
```

```ini
[users]
    default-home = /home/%U
    default-shell = /bin/bash

[active-directory]
    default-client = sssd
    os-name = Linux Mint
    os-version = 20

[service]
    automatic-install = no

[awesome.com]
    fully-qualified-names = yes
    automatic-id-mapping = no
    user-principal = yes
    manage-system = yes
```

Edit `timesyncd.conf`.

```bash
sudo xed /etc/systemd/timesyncd.conf
```

```ini
[Time]
NTP=asmdc7.awesome.com
#FallbackNTP=ntp.ubuntu.com
#RootDistanceMaxSec=5
#PollIntervalMinSec=32
#PollIntervalMaxSec=2048
```

```bash
sudo timedatectl set-ntp true
sudo systemctl restart systemd-timesyncd.service
sudo timedatectl --adjust-system-clock
timedatectl status
```

Discover the realm you want to join.

```bash
realm discover awesome.com
```

Test your credentials.

```bash
kinit avawesome
klist
kdestroy
```

Join the Active Directory Domain.

```bash
sudo realm join --verbose --user=aawesome-a --computer-ou=OU=Computers,OU="ASM Headquarters",DC=awesome,DC=com awesome.com
```

Edit `sssd.conf`.
```bash
sudo xed /etc/sssd/sssd.conf
```

```ini
[sssd]
domains = awesome.com
config_file_version = 2
services = nss, pam

[domain/awesome.com]
ad_domain = awesome.com
krb5_realm = AWESOME.COM
realmd_tags = manages-system joined-with-adcli
cache_credentials = True
id_provider = ad
krb5_store_password_if_offline = True
default_shell = /bin/bash
ldap_id_mapping = True
use_fully_qualified_names = True
fallback_homedir = /home/%u
access_provider = ad
ad_hostname = [[computer_name.my-domain]].com
dyndns_update = True
dyndns_refresh_interval = 43200
dyndns_update_ptr = True
dyndns_ttl = 3600
dyndns_auth = GSS-TSIG
```

Restart the SSSD Service.

```bash
sudo systemctl restart sssd.service
```

Finally, [update your login screen](#step-7-modify-your-login-window).

Restart your computer, and you should able to log in using your Active Directory Credentials.

**Update 8/30/2019:**  [Click here](#update-8302019) to see some optional, final steps you could take.

## The Rest of the Story

I work for a "Windows Shop", or ".Net Shop".  Call it what you like, but it boils down to no Linux to be found.  Fortunately, my work environment is flexible, so I decided to take on the challenge to put Linux only on my work laptop, and get it up and running.

Things like [snaps](https://snapcraft.io/store "Snapcraft.io") have been extremely helpful in getting the software I need on my machine, and [LibreOffice](https://www.libreoffice.org/download/download/ "Download LibreOffice") has come a long way.  Everything else, like Office 365 for email, just needs a web browser.  And in case I run into any edge cases, I do have Windows 10 on a VirtualBox image, but all that is outside of the scope of this article.

My very first step to tackle was Windows Active Directory Integration with a Linux machine; I needed to be able to use my Active Directory credentials to log into my Linux laptop.

Spoiler alert.  I got it to work! :-)  And I'm very excited to be able to use Linux at home and now at work!

Read on to see how I did it.

### Steps Toward Awesome

First, I would like to give credit where credit is due.  [This article](https://bitsofwater.com/2018/05/08/join-ubuntu-18-04-to-active-directory/ "Join Ubuntu to Active Directory") was extremely helpful in guiding me in getting everything set up.

#### Step 0: Install the Needed Packages

Open up your terminal, and enter the text below to get the needed packages installed.

```bash
sudo apt install realmd sssd sssd-tools libnss-sss libpam-sss krb5-user adcli samba-common-bin oddjob oddjob-mkhomedir packagekit samba python-dnspython
```

The `krb5-user` package will prompt for the Active Directory "realm", and you'll want to enter your realm in all CAPS.

![krb5 User Pacage Install](/posts/join-linux-mint-19-to-an-active-directory-domain/krb5-user-package-install.png "krb5-user Package Install")

After I installed the packages, I went ahead and restarted my machine.

#### Step 1: Edit Your krb5.conf File

Start by opening `krb5.conf`:

```bash
sudo xed /etc/krb5.conf
```

You can replace the contents of the current file with the text below.

***Note:*** *Values inside the double brackets (i.e. - [[value]]), need to be replaced with the correct values for your environment.  Replace the value, and additionally, remove the double brackets. (i.e. - domain = [[my-domain.com]] --> domain = awesome.com)*

```ini
[libdefaults]
    default_realm = [[YOUR-REALM.COM]]  #YOUR-REALM.COM should be in CAPS
    dns_lookup_kdc = true
    dns_lookup_realm = true

[realms]
    [[YOUR-REALM.COM]] = {              #replace value, remove double brackets
        kdc = [[your-realm.com]]
        admin_server = [[your-realm.com]]
        master_kdc = [[your-realm.com]]
        default_domain = [[your-domain.com]]  #my domain and realm were the same
        }

[domain_realm]
    .[[your-domain.com]] = [[YOUR-REALM.COM]] #YOUR-REALM.COM should be in CAPS
    [[your-domain.com]] = [[YOUR-REALM.COM]]

[logging]
    kdc = SYSLOG:INFO
    admin_server = FILE=/var/kadm5.log
```

To finish up this step, run:

```bash
sudo pam-auth-update
```

#### Step 2: Edit Your realmd.conf File

Now open up your `realmd.conf` file.

```bash
sudo xed /etc/realmd.conf
```

Copy and paste the text below into the file.  You can of course replace the values for "*os-name*" and "*os-version*".

```ini
[users]
    default-home = /home/%U
    default-shell = /bin/bash

[active-directory]
    default-client = sssd
    os-name = [[Linux Mint]] #you can put your Linux Distribution Name
    os-version = [[20]]    #you can put your Distribution Version

[service]
    automatic-install = no

[my-domain.com]   #replace my-domain.com, but KEEP the brackets on this one
    fully-qualified-names = yes
    automatic-id-mapping = no
    user-principal = yes
    manage-system = yes
```

#### Step 3: Edit Your timesyncd.conf File

You should be used to editing files by now in this tutorial, so here we go again.  Open up your terminal, and enter the text below.

```bash
sudo xed /etc/systemd/timesyncd.conf
```

All you need to do is change the "*NTP*" value to the address of your local Network Time Protocol (NTP) Server.  You may have to ask your Network Administrator for the server address, and if you are the Network Administrator, I hope you know the address of your NTP Server ;-)

```ini
[Time]
NTP=[[my-ntp-server.my-domain.com]]     #replace value, remove double brackets
#FallbackNTP=ntp.ubuntu.com
#RootDistanceMaxSec=5
#PollIntervalMinSec=32
#PollIntervalMaxSec=2048
```

Now you'll need to update your local network time.  Your local computer time needs to be within five minutes of the Kerberos (authentication) Server.  So the clock times need to match, or you won't be able to log in.

You'll need to run the following commands in order to make sure your date and time are up-to-date.

```bash
sudo timedatectl set-ntp true
sudo systemctl restart systemd-timesyncd.service
sudo timedatectl --adjust-system-clock
```

Now you can check the status of your local date and time synchronization.

```bash
timedatectl status
```

And your results should be similar to the screenshot below.

![Sync with Time Server](/posts/join-linux-mint-19-to-an-active-directory-domain/sync-with-time-server.png "Date Time Sync")

#### Step 4: Test Your Credentials

Even though your computer may not be bound to the Active Directory yet, you can now test your login credentials to make sure everything is set up correctly so far.

Run the command below.

```bash
realm discover [[my-domain.com]]
```

Successful results should look similar to the output below.

```bash
my-domain.com
  type: kerberos
  realm-name: MY-REALM.COM
  domain-name: my-domain.com
  configured: kerberos-member
  server-software: active-directory
  client-software: sssd
  required-package: sssd-tools
  required-package: sssd
  required-package: libnss-sss
  required-package: libpam-sss
  required-package: adcli
  required-package: samba-common-bin
  login-formats: %U@my-domain.com
  login-policy: allow-realm-logins
```

You can now try to "test" your login credentials.  Do that by running the commands below, and enter your Active Directory password when prompted.

```bash
kinit [[my-user-name]]
```

You can verify that your login attempt worked by running this next command.

```bash
klist
```

If that worked, your results should look similar to the screenshot below.

![klist Command](/posts/join-linux-mint-19-to-an-active-directory-domain/klist.png "klist Command")

Be sure to destroy your Kerberos token when you're done.

```bash
kdestroy
```

#### Step 5: Join the Active Directory Domain

Time to join your Active Directory.  You'll need a Network Administrator, or someone with a Network Admin username/password in order to get your computer joined to the Active Directory realm.

Enter the text below into your terminal, and don't forget to replace the values in the double brackets (along with the brackets).

```bash
sudo realm join --verbose --user=[[network-admin-username]] --computer-ou=OU=[[Computers]],OU="[[Active-Directory-OU-Value]]",DC=[[my-domain-without-the-com]],DC=com [[my-domain.com]]
```

You'll enter the Network Administrator username, and you'll be prompted for the password.

```bash
 * Resolving: _ldap._tcp.my-domain.com
 * Performing LDAP DSE lookup on: 10.1.1.14
 * Performing LDAP DSE lookup on: 10.1.1.15
 * Performing LDAP DSE lookup on: 10.1.1.28
 * Successfully discovered: my-domain.com
Password for awesome-admin:
```

After the Network Administrator password has been entered, the rest of the output should look similar to this:

```bash
 * Unconditionally checking packages
 * Resolving required packages
 * LANG=C /usr/sbin/adcli join --verbose --domain my-domain.com --domain-realm MY-REALM.COM --domain-controller 10.1.1.14 --computer-ou OU=Computers,OU=SHORT-DOMAIN-NAME Headquarters,DC=domain-controller,DC=com --os-name Linux Mint --os-version 19.1 --login-type user --login-user network-admin-username --stdin-password --user-principal
 * Using domain name: my-domain.com
 * Calculated computer account name from fqdn: COMPUTER-NAME
 * Using domain realm: my-domain.com
 * Sending netlogon pings to domain controller: cldap://10.1.1.14
 * Received NetLogon info from: SERVER.my-domain.com
 * Wrote out krb5.conf snippet to /var/cache/realmd/adcli-xyzab-oFOwIT/krb5.d/adcli-krb5-conf-wCGqIO
 * Authenticated as user: network-admin-username@MY-REALM.COM
 * Looked up short domain name: SHORT-DOMAIN-NAME
 * Using fully qualified name: computer-name
 * Using domain name: my-domain.com
 * Using computer account name: COMPUTER-NAME
 * Using domain realm: my-domain.com
 * Calculated computer account name from fqdn: COMPUTER-NAME
 * With user principal: host/computer-name@MY-REALM.COM
 * Generated 120 character computer password
 * Using keytab: FILE:/etc/krb5.keytab
 * Found computer account for COMPUTER-NAME$ at: CN=COMPUTER-NAME,OU=Computers,OU=SHORT-DOMAIN-NAME Headquarters,DC=my-domain,DC=com
 * Set computer password
 * Retrieved kvno '7' for computer account in directory: CN=COMPUTER-NAME,OU=Computers,OU=SHORT-DOMAIN-NAME Headquarters,DC=my-domain,DC=com
 * Modifying computer account: userAccountControl
 * Modifying computer account: operatingSystemVersion, operatingSystemServicePack
 * Modifying computer account: userPrincipalName
 * Discovered which keytab salt to use
 * Added the entries to the keytab: COMPUTER-NAME$@MY-REALM.COM: FILE:/etc/krb5.keytab
 * Added the entries to the keytab: host/COMPUTER-NAME@MY-REALM.COM: FILE:/etc/krb5.keytab
 * Added the entries to the keytab: host/COMPUTER-NAME@MY-REALM.COM: FILE:/etc/krb5.keytab
 * Cleared old entries from keytab: FILE:/etc/krb5.keytab
 * Added the entries to the keytab: host/computer-name@MY-REALM.COM: FILE:/etc/krb5.keytab
 * Added the entries to the keytab: RestrictedKrbHost/COMPUTER-NAME@MY-REALM.COM: FILE:/etc/krb5.keytab
 * Added the entries to the keytab: RestrictedKrbHost/computer-name@MY-REALM.COM: FILE:/etc/krb5.keytab
 * /usr/sbin/update-rc.d sssd enable
 * /usr/sbin/service sssd restart
 * Successfully enrolled machine in realm
```

#### Step 6: Edit Your sssd.conf File

Open up `sssd.conf` for editing

```bash
sudo xed /etc/sssd/sssd.conf
```

Copy and paste the text below into the file.  Again, don't forget to replace the values in the double brackets (along with the brackets).

```
[sssd]
domains = [[my-domain]].com
config_file_version = 2
services = nss, pam

[domain/[[my-domain]].com]
ad_domain = [[my-domain]].com
krb5_realm = [[MY-REALM]].COM
realmd_tags = manages-system joined-with-adcli
cache_credentials = True
id_provider = ad
krb5_store_password_if_offline = True
default_shell = /bin/bash
ldap_id_mapping = True
use_fully_qualified_names = True
fallback_homedir = /home/%u
access_provider = ad
ad_hostname = [[computer_name.my-domain]].com
dyndns_update = True
dyndns_refresh_interval = 43200
dyndns_update_ptr = True
dyndns_ttl = 3600
dyndns_auth = GSS-TSIG
```

Save the file, then close the text editor, and run the command below.

```bash
sudo systemctl restart sssd.service
```

#### Step 7: Modify Your Login Window

And now for our last step.  This is specific for Linux Mint.

Start by opening the Login Window settings, as seen in the screenshot below.

![Login Window 1](/posts/join-linux-mint-19-to-an-active-directory-domain/login-window-01.png "Open Login Window")

Now make sure you settings look like this:

![Login Window 2](/posts/join-linux-mint-19-to-an-active-directory-domain/login-window-02.png "Login Window Settings")

## Conclusion

Those are the steps.  If anything is unclear, let me know in the comments below.  Hope this is helpful :-)

## Update 8/30/2019

So this is an optional step that may not have to happen in every scenario.  Upon logging in with your Active Directory User, you may discover that you do not have the rights to do anything.  So, you will have to log in with a *local* administrator account, and add your *domain* account to the following groups by running these commands:
```bash
sudo usermod -a -G adm aaronvon@awesome.com
sudo usermod -a -G cdrom aaronvon@awesome.com
sudo usermod -a -G dip aaronvon@awesome.com
sudo usermod -a -G lpadmin aaronvon@awesome.com
sudo usermod -a -G plugdev aaronvon@awesome.com
sudo usermod -a -G sambashare aaronvon@awesome.com
sudo usermod -a -G sudo aaronvon@awesome.com
```

End of Line.
