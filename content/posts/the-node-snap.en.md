---
title: "The Node Snap"
date: 2020-01-07T11:05:59-05:00
lastmod: 2020-01-07T11:05:59-05:00
draft: false
comment:
  enable: true
description: "If you are getting the 'cannot create user data directory: /nonexistent/snap/node...' error, then try this fix."
categories:
  - "Snap Packages"
  - NodeJS
---

## TL;DR

If you installed the [NodeJS Snap package](https://snapcraft.io/node "Node Snap Package"), and while trying to install an npm package you recieve this error:

> WARNING: cannot create user data directory: cannot create "/nonexistent/snap/node/xxxx": mkdir /nonexistent: permission denied
cannot create user data directory: /nonexistent/snap/node/xxxx: Permission denied

Run this command to fix it:
```bash
sudo npm config set scripts-prepend-node-path true
```

## The Rest of the Story

I have the [NodeJS Snap package](https://snapcraft.io/node "Node Snap Package") installed on a Linux Mint 19.3 PC.  I wanted to install the `katacoda-cli` npm package, but I kept getting this error:

> WARNING: cannot create user data directory: cannot create "/nonexistent/snap/node/xxxx": mkdir /nonexistent: permission denied
cannot create user data directory: /nonexistent/snap/node/xxxx: Permission denied

While searching for an anwser, I found that the error is **not** specific to the `katacoda-cli` npm package, so this should apply to other packages producing the same error.

I would like to reiterate, that I am using the NodeJS **Snap Package**.  Using the standard deb/apt package to install NodeJS does not produce this error.

So, in order to resolve the NodeJS Snap error, you just need to run the following command:

```bash
sudo npm config set scripts-prepend-node-path true
```

After that, I was able to install `katacoda-cli` npm package:

```bash
sudo npm install katacoda-cli --global
```

All credit for this solution goes to the [this GitHub issue comment](https://github.com/nwjs/npm-installer/issues/73#issuecomment-566842018 "Can't install using NodeJS snap").  Thanks a bunch to the poster!

That's all for this post; short and sweet!

End of Line.
