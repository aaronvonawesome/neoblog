---
title: "VLC 3 on Linux Mint 18"
date: 2018-08-28T22:25:00-05:00
lastmod: 2018-08-28T22:25:00-05:00
draft: false
comment:
  enable: true
description: "If you want to listen to ad-free music on YouTube, for example, on your Linux Mint 18.3 box, you will need to install VLC 3, and here is how you can do that with a Snap."
categories:
  - "Linux Mint"
  - VLC
  - "Snap Packages"
---

## TL;DR
If you want to listen to ad-free [Chillstep](https://www.youtube.com/watch?v=fWRISvgAygU "Epic Chillstep Collection 2015") with Linux Mint 18.3, you will need to install VLC 3, and here is how you can do that with a Snap:
```bash
sudo apt install snapd
sudo snap install vlc
```

## The Rest of the Story

I'll admit, not much of a first post, but I have to start somewhere.

Ok, so here's the scoop; I have Linux Mint 18.3 installed, and I would like to listen to some [Chillstep](https://www.youtube.com/watch?v=fWRISvgAygU "Epic Chillstep Collection 2015") on YouTube while I'm working, but without the interruption of ads.

So, easy, just need to open up VLC, and then open a network stream:
<br />
<br />
![Open Network Stream 1](/posts/vlc-3-on-linux-mint-18/VLC-Open-Network-Stream-01.png "Open Network Stream: Step 1")

![Open Network Stream 2](/posts/vlc-3-on-linux-mint-18/VLC-Open-Network-Stream-02.png "Open Network Stream: Step 2")

After that I can enjoy the music, right?  Wrong.

Unfortunately with VLC 2.2.2, the default version install on Linux Mint 18.3, I get this error:

> Your input can't be opened:
>
> VLC is unable to open the MRL 'https://...

What this means is that YouTube is forcing https (good thing) on all of their connections.  However, "Network Stream" option in VLC does not like that...until version 3.

Alright, now I just need to install VLC 3.  Yet another unfortunate turn, as the [Videolan team PPA](https://launchpad.net/~videolan/+archive/ubuntu/stable-daily "Videolan team PPA") does not provide VLC version 3.

### The Solution
I am fairly new to Sanps, Flatpaks, AppImages, etc,.  Regardless, I decided to give Snaps a shot, and here is how you install VLC 3 with a Snap on Linux Mint 18.3:

```bash
sudo apt install snapd
sudo snap install vlc
```

After that install completes, you will have the Snap with VLC 3 installed as well as the original VLC that came with Linux Mint 18.3 (2.2.2):
<br />
![Open Network Stream 2](/posts/vlc-3-on-linux-mint-18/VLC-2-and-3-Installed.png "Note: If you do not see both versions in your launcher, pull a Windows and reboot")

Now just repeat my steps above to "Open Network Stream...", but this time with your newly installed VLC 3, and you'll be listening to your ad-free chillstep in no time.

Hope you found this helpful, and if you have any questions, please comment below.

End of Line.
